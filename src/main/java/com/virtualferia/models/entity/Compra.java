package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "compras")
public class Compra implements Serializable{

	
	@Id
	@SequenceGenerator(name = "SEQ_API_VENTA", sequenceName = "SEQ_API_VENTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_VENTA")
	private Long idCompra;
	
    private Double costoTotal;
    
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaInicio;
    
    @Column(name = "id_comprador")
    private Long id_comprador;
    

	@OneToOne( fetch = FetchType.EAGER)
    @JoinColumn(name = "id_estado_venta")
    private EstadoVenta estadoVenta;
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(  fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_compra")
    private List<RegistroVenta> registrosCompra;
    
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_subasta")
    private Subasta subasta;
    

	
    private static final long serialVersionUID = 1L;
	
    public Compra() {
    	this.idCompra = null;
    	this.costoTotal = 0.0 ;
    	this.fechaInicio = null;
    	this.estadoVenta = null;
    	this.id_comprador = null;
    	this.registrosCompra = new ArrayList<RegistroVenta>();
	}    
    

	public Compra(Long idCompra, Double costoTotal, Date fechaInicio, Long id_comprador, EstadoVenta estadoVenta,
			List<RegistroVenta> registrosCompra, Subasta subasta) {
		super();
		this.idCompra = idCompra;
		this.costoTotal = costoTotal;
		this.fechaInicio = fechaInicio;
		this.id_comprador = id_comprador;
		this.estadoVenta = estadoVenta;
		this.registrosCompra = registrosCompra;
		this.subasta = subasta;
	}

	public Long getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(Long idCompra) {
		this.idCompra = idCompra;
	}

	public Double getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(Double costoTotal) {
		this.costoTotal = costoTotal;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Long getId_comprador() {
		return id_comprador;
	}
	public void setId_comprador(Long id_comprador) {
		this.id_comprador = id_comprador;
	}

	public EstadoVenta getEstadoVenta() {
		return estadoVenta;
	}

	public void setEstadoVenta(EstadoVenta estadoVenta) {
		this.estadoVenta = estadoVenta;
	}

	public List<RegistroVenta> getRegistrosCompra() {
		return registrosCompra;
	}

	public void setRegistrosCompra(List<RegistroVenta> registrosCompra) {
		this.registrosCompra = registrosCompra;
	}


	public Subasta getSubasta() {
		return subasta;
	}

	public void setSubasta(Subasta subasta) {
		this.subasta = subasta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

    
    
	
}
