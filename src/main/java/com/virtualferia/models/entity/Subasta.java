package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "subastas")
public class Subasta implements Serializable{
	
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SEQ_API_SUBASTA", sequenceName = "SEQ_API_SUBASTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_SUBASTA")
    private Long idSubasta;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaInicio;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaFin;
	
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany( fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_subasta")
    private List<RegistroSubasta> registros;
    
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  	@OneToOne(fetch = FetchType.EAGER)
  	@JoinColumn(name = "idRegistroSubasta")
    private RegistroSubasta SubastaSeleccionada;
    
    
//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//	@OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
//    @JoinColumn(name = "id_compra")
    
    @Column(name ="id_compra")
    private Long compra;
    

    
    
    
	public Subasta() {
		this.registros = new ArrayList<RegistroSubasta>();
		// this.SubastaSeleccionada = new RegistroSubasta();
		// TODO Auto-generated constructor stub
	}
	 

	public Subasta(Long idSubasta, Date fechaInicio, Date fechaFin, List<RegistroSubasta> registros,
			RegistroSubasta subastaSeleccionada, Long compra) {
		super();
		this.idSubasta = idSubasta;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.registros = registros;
		SubastaSeleccionada = subastaSeleccionada;
		this.compra = compra;
	}


	public Long getIdSubasta() {
		return idSubasta;
	}

	public void setIdSubasta(Long idSubasta) {
		this.idSubasta = idSubasta;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<RegistroSubasta> getRegistros() {
		return registros;
	}

	public void setRegistros(List<RegistroSubasta> registros) {
		this.registros = registros;
	}

	public RegistroSubasta getSubastaSeleccionada() {
		return SubastaSeleccionada;
	}

	public void setSubastaSeleccionada(RegistroSubasta subastaSeleccionada) {
		SubastaSeleccionada = subastaSeleccionada;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Long getCompra() {
		return compra;
	}

	public void setCompra(Long compra) {
		this.compra = compra;
	}




 
	
	
	
    
	
    
	
    
}
