package com.virtualferia.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "transportistas")
public class Transportista implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_TRANSPORTISTA", sequenceName = "SEQ_API_TRANSPORTISTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_TRANSPORTISTA")
    private Long idTransportista;
	@OneToOne()
	@JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @OneToOne()
    @JoinColumn(name = "id_transporte")
    private Transporte transporte;
    
	public Transportista() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Transportista(Long idTransportista, Usuario usuario, Transporte transporte) {
		super();
		this.idTransportista = idTransportista;
		this.usuario = usuario;
		this.transporte = transporte;
	}
	
	public Long getIdTransportista() {
		return idTransportista;
	}
	public void setIdTransportista(Long idTransportista) {
		this.idTransportista = idTransportista;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Transporte getTransporte() {
		return transporte;
	}
	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
    
    
    
}
