package com.virtualferia.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "perfil_usuarios")
public class PerfilUsuario implements Serializable{
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_PERFIL_USUARIO", sequenceName = "SEQ_API_PERFIL_USUARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_PERFIL_USUARIO")
	private Long idPerfilUsuario;
    private String nombre;
    private String descripcion;
    
    
    
    
	public PerfilUsuario() {
		// TODO Auto-generated constructor stub
	}
	public PerfilUsuario(Long idPerfil, String nombre, String descripcion) {
		
		this.idPerfilUsuario = idPerfil;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	public Long getIdPerfil() {
		return idPerfilUsuario;
	}
	public void setIdPerfil(Long idPerfil) {
		this.idPerfilUsuario = idPerfil;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
    
    
}
