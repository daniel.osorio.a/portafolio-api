package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pagosDistribucion")

public class PagoDistribucion  implements Serializable{
	

	@Id
	@SequenceGenerator(name = "SEQ_API_DISTRIBUCIONPAPGO", sequenceName = "SEQ_API_DISTRIBUCIONPAPGO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_DISTRIBUCIONPAPGO")
	Long idPagoDistribucion ;
	
	
	@Column(name = "id_pago")
	Long id_pago;
	
	private Double monto;
	
//	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//	@OneToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_usuario")
//	private Usuario usuario;
	
	@Column(name="id_usuario")
	private Long usuario;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaRegistro;
	
	
	

	public PagoDistribucion() {
		// TODO Auto-generated constructor stub
	}

	public PagoDistribucion(Long idPagoDistribucion, Long id_pago, Double monto, Long usuario, Date fechaRegistro) {
		super();
		this.idPagoDistribucion = idPagoDistribucion;
		this.id_pago = id_pago;
		this.monto = monto;
		this.usuario = usuario;
		this.fechaRegistro = fechaRegistro;
	}

	public Long getIdPagoDistribucion() {
		return idPagoDistribucion;
	}

	public void setIdPagoDistribucion(Long idPagoDistribucion) {
		this.idPagoDistribucion = idPagoDistribucion;
	}

	public Long getId_pago() {
		return id_pago;
	}

	public void setId_pago(Long id_pago) {
		this.id_pago = id_pago;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Long getUsuario() {
		return usuario;
	}

	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	private static final long serialVersionUID = 1L;
	
	

}
