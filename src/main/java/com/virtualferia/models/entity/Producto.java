package com.virtualferia.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "productos")
public class Producto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_PRODUCTO", sequenceName = "SEQ_API_PRODUCTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_PRODUCTO")
	private Long idProducto;
    private String nombre;
    private  Double volumen;
    private Double peso;
    
    private String value;
    
	public Producto() {
		this.idProducto = null;
		this.nombre = "";
		this.volumen = 0.0;
		this.peso = 0.0;
		// TODO Auto-generated constructor stub
	}
	public Producto(Long idProducto, String nombre, Double volumen, Double peso) {
		
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.volumen = volumen;
		this.peso = peso;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getVolumen() {
		return volumen;
	}
	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getValue() {
		return nombre;
	}
	 
    
    
    
}
