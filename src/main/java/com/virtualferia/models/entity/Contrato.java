package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "contratos")
public class Contrato implements Serializable{

	@Id
	@SequenceGenerator(name = "SEQ_API_CONTRATO", sequenceName = "SEQ_API_CONTRATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_CONTRATO")
	private Long idContrato;
    private String nombre ;
    private String descripcion;
    
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaInicio ;
    
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaTermino ;

    
    
    public Contrato() {
	 
		// TODO Auto-generated constructor stub
	}



	public Contrato(Long idContrato, String nombre, String descripcion, Date fechaInicio, Date fechaTermino) {
		this.idContrato = idContrato;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaInicio = fechaInicio;
		this.fechaTermino = fechaTermino;
	}



	public Long getIdContrato() {
		return idContrato;
	}



	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public Date getFechaInicio() {
		return fechaInicio;
	}



	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}



	public Date getFechaTermino() {
		return fechaTermino;
	}



	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	private static final long serialVersionUID = 1L;

}
