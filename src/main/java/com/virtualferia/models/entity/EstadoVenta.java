package com.virtualferia.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "estado_ventas")
public class EstadoVenta implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_ESTADO_VENTA", sequenceName = "SEQ_API_ESTADO_VENTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_ESTADO_VENTA")
	private Long idEstadoVenta;
    private String nombre;
    private String descripcion;
    
    
	public EstadoVenta() {
		 
		// TODO Auto-generated constructor stub
	}
	
	public EstadoVenta(Long idEstadoVenta, String nombre, String descripcion) {
		
		this.idEstadoVenta = idEstadoVenta;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	public Long getIdEstadoVenta() {
		return idEstadoVenta;
	}
	public void setIdEstadoVenta(Long idEstadoVenta) {
		this.idEstadoVenta = idEstadoVenta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    

}
