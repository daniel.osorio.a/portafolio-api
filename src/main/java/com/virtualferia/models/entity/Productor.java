package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "productores")
public class Productor implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_PRODUCTOR", sequenceName = "SEQ_API_PRODUCTOR", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_PRODUCTOR")
	private Long idProductor;
	
	@OneToOne()
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@OneToMany(  fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "id_productor")
    private List<RegistroVentaProductor> registroVentas;
	
    
    
    
    public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
	public Productor() {
		this.registroVentas = new ArrayList<RegistroVentaProductor>();
		// TODO Auto-generated constructor stub
	}
	 
	
	
	public Productor(Long idProductor, Usuario usuario, List<RegistroVentaProductor> registroVentas) {
		super();
		this.idProductor = idProductor;
		this.usuario = usuario;
		this.registroVentas = registroVentas;
	}

	public Long getIdProductor() {
		return idProductor;
	}
	public void setIdProductor(Long idProductor) {
		this.idProductor = idProductor;
	}
	public List<RegistroVentaProductor> getVentas() {
		return registroVentas;
	}
	public void setVentas(List<RegistroVentaProductor> ventas) {
		this.registroVentas = ventas;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<RegistroVentaProductor> getRegistroVentas() {
		return registroVentas;
	}

	public void setRegistroVentas(List<RegistroVentaProductor> registroVentas) {
		this.registroVentas = registroVentas;
	}
    
	
    
}
