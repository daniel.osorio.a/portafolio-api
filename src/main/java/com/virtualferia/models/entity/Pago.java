package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pagos")
public class Pago implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_PAGO", sequenceName = "SEQ_API_PAGO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_PAGO")
	private Long idPago ;
	
    private Double monto;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaRegistro;
    
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaPago;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_compra")
    private Compra compra ;
    
    private boolean pagado;
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(  fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pago")
    private List<PagoDistribucion> Pagodistribucion;
    
    
	public Pago(Long idPago, Double monto, Date fechaRegistro, Date fechaPago, Compra compra, boolean pagado,
			List<PagoDistribucion> pagodistribucion) {
		super();
		this.idPago = idPago;
		this.monto = monto;
		this.fechaRegistro = fechaRegistro;
		this.fechaPago = fechaPago;
		this.compra = compra;
		this.pagado = pagado;
		Pagodistribucion = pagodistribucion;
	}


	public Pago() {
		
		// TODO Auto-generated constructor stub
	}
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Compra getCompra() {
		return compra;
	}
	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	public boolean isPagado() {
		return pagado;
	}
	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}
	public List<PagoDistribucion> getPagodistribucion() {
		return Pagodistribucion;
	}
	public void setPagodistribucion(List<PagoDistribucion> pagodistribucion) {
		Pagodistribucion = pagodistribucion;
	}
	
	
	
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
    
    

}
