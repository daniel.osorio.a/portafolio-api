package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_USUARIO", sequenceName = "SEQ_API_USUARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_USUARIO")
	private Long idUsuario;
    private String nombreUsuario;
    private String email;
    private String contrasena;
    private Integer telefono;
    private boolean activo;
    
    @OneToOne()
    @JoinColumn(name = "id_perfil_usuario")
    private PerfilUsuario perfilUsuario;
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_contrato")
    private Contrato contrato;
    
    @OneToMany( fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario")
    private List<PagoDistribucion> pagos;
	
    public Usuario() {
    	this.pagos = new ArrayList<PagoDistribucion>();
		// TODO Auto-generated constructor stub
	}

	public Usuario(Long idUsuario, String nombreUsuario, String email, String contrasena, Integer telefono,
			PerfilUsuario perfilUsuario, Contrato contrato, List<PagoDistribucion> pagos, boolean activo) {
		
		this.idUsuario = idUsuario;
		this.nombreUsuario = nombreUsuario;
		this.email = email;
		this.contrasena = contrasena;
		this.telefono = telefono;
		this.perfilUsuario = perfilUsuario;
		this.contrato = contrato;
		this.pagos = pagos;
		this.activo = activo;
	}

	
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public PerfilUsuario getPerfilUsuario() {
		return perfilUsuario;
	}

	public void setPerfilUsuario(PerfilUsuario perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public List<PagoDistribucion> getPagos() {
		return pagos;
	}

	public void setPagos(List<PagoDistribucion> pagos) {
		this.pagos = pagos;
	}

	
	
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    





}
