package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "registro_subastas")
public class RegistroSubasta implements Serializable {

	@Id
	@SequenceGenerator(name = "SEQ_API_REG_SUBASTA", sequenceName = "SEQ_API_REG_SUBASTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_REG_SUBASTA")
	private Long idRegistroSubasta;

	private Date fechaRegistro;
	
    @Column(name = "id_transporte")
    private Long id_transporte;
    
    @Column(name = "id_subasta")
    private Long id_subasta;

	

	private static final long serialVersionUID = 1L;

	public RegistroSubasta() {
		 
	}
	
	

	public RegistroSubasta(Long idRegistroSubasta, Date fechaRegistro, Long id_transporte, Long id_subasta) {
		super();
		this.idRegistroSubasta = idRegistroSubasta;
		this.fechaRegistro = fechaRegistro;
		this.id_transporte = id_transporte;
		this.id_subasta = id_subasta;
	}




	public Long getIdRegistroSubasta() {
		return idRegistroSubasta;
	}

	public void setIdRegistroSubasta(Long idRegistroSubasta) {
		this.idRegistroSubasta = idRegistroSubasta;
	}


	
	
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Long getId_transporte() {
		return id_transporte;
	}

	public void setId_transporte(Long id_transporte) {
		this.id_transporte = id_transporte;
	}

	public Long getId_subasta() {
		return id_subasta;
	}

	public void setId_subasta(Long id_subasta) {
		this.id_subasta = id_subasta;
	}

}
