package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "transportes")
public class Transporte implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "SEQ_API_TRANSPORTE", sequenceName = "SEQ_API_TRANSPORTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_TRANSPORTE")
    private Long idTransporte;
    private Double tamano;
    private Double capacidad;
    private Boolean regrigeracion;
    private Double precio;

    @OneToMany( fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "id_transporte")
    private List<RegistroSubasta> subastas;
    
	public Transporte(Long idTransporte, Double tamano, Double capacidad, Boolean regrigeracion, Double precio,
			List<RegistroSubasta> subastas) {
		
		this.idTransporte = idTransporte;
		this.tamano = tamano;
		this.capacidad = capacidad;
		this.regrigeracion = regrigeracion;
		this.precio = precio;
		this.subastas = subastas;
	}

	public Transporte() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdTransporte() {
		return idTransporte;
	}

	public void setIdTransporte(Long idTransporte) {
		this.idTransporte = idTransporte;
	}

	public Double getTamano() {
		return tamano;
	}

	public void setTamano(Double tamano) {
		this.tamano = tamano;
	}

	public Double getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Double capacidad) {
		this.capacidad = capacidad;
	}

	public Boolean getRegrigeracion() {
		return regrigeracion;
	}

	public void setRegrigeracion(Boolean regrigeracion) {
		this.regrigeracion = regrigeracion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public List<RegistroSubasta> getSubastas() {
		return subastas;
	}

	public void setSubastas(List<RegistroSubasta> subastas) {
		this.subastas = subastas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
    
    
    
}
