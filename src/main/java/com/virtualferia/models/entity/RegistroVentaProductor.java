package com.virtualferia.models.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "registro_venta_productores")
public class RegistroVentaProductor implements Serializable{
	
	@Id
	@SequenceGenerator(name = "SEQ_API_REGVENPRODTES", sequenceName = "SEQ_API_REGVENPRODTES", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_REGVENPRODTES")
	private Long idRegistroVentaProductor;
	
	private Integer cantidad;
	private Double precio;
	
	@Column(name="id_registro_venta")
	private Long id_registro_venta;
	
	@Column(name = "id_productor")
	private Long id_productor;


	

	private static final long serialVersionUID = 1L;

	public RegistroVentaProductor(Long idRegistroVentaProductor, Long id_productor,	Integer cantidad, Double precio
			,Long id_registro_venta) {
		this.idRegistroVentaProductor = idRegistroVentaProductor;
//		this.producto = producto;
//		this.id_venta = id_venta;
		this.id_productor = id_productor;
		this.cantidad = cantidad;
		this.precio = precio;
		this.id_registro_venta = id_registro_venta;
	}

	public Long getId_registro_venta() {
		return id_registro_venta;
	}

	public void setId_registro_venta(Long id_registro_venta) {
		this.id_registro_venta = id_registro_venta;
	}

	public RegistroVentaProductor() {
		this.idRegistroVentaProductor = null;
		this.id_registro_venta = null;
		this.id_productor = null;
		this.cantidad = null;
		this.precio = 0.0;
	}

	public Long getIdRegistroVentaProductor() {
		return idRegistroVentaProductor;
	}

	public void setIdRegistroVentaProductor(Long idRegistroVentaProductor) {
		this.idRegistroVentaProductor = idRegistroVentaProductor;
	}

//	public Producto getProducto() {
//		return producto;
//	}
//
//	public void setProducto(Producto producto) {
//		this.producto = producto;
//	}

//	public Long getId_venta() {
//		return id_venta;
//	}
//
//	public void setId_venta(Long id_venta) {
//		this.id_venta = id_venta;
//	}

	public Long getId_productor() {
		return id_productor;
	}

	public void setId_productor(Long id_productor) {
		this.id_productor = id_productor;
	}


	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
