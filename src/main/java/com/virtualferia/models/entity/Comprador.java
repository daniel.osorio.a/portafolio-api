package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "compradores")
public class Comprador implements Serializable {
     
	@Id
	@SequenceGenerator(name = "SEQ_API_Comprador", sequenceName = "SEQ_API_Comprador", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_Comprador")
	private Long idComprador;
	
	@OneToOne()
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	
    @OneToMany(  fetch = FetchType.LAZY  )
    @JoinColumn(name = "id_comprador" , updatable = false, insertable = false, nullable=false)
    private List<Compra> ventas;
    
    

	private static final long serialVersionUID = 1L;

	
	


	public Comprador() {
		this.ventas = new ArrayList<Compra>();
		// TODO Auto-generated constructor stub
	}



	public Comprador(Long idComprador, Usuario usuario, List<Compra> ventas) {
		this.idComprador = idComprador;
		this.usuario = usuario;
		this.ventas = ventas;
	}



	public Long getIdComprador() {
		return idComprador;
	}



	public void setIdComprador(Long idComprador) {
		this.idComprador = idComprador;
	}



	public Usuario getUsuario() {
		return usuario;
	}



	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}




	public List<Compra> getVentas() {
		return ventas;
	}



	public void setVentas(List<Compra> ventas) {
		this.ventas = ventas;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}
