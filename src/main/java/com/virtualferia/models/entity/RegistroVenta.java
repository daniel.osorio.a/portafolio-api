package com.virtualferia.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "registro_ventas")
public class RegistroVenta implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "SEQ_API_REGVENTA", sequenceName = "SEQ_API_REGVENTA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_API_REGVENTA")
    private Long idRegistroVenta;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_producto")
    private Producto producto;

	private Integer cantidad;
    
    @Column(name = "id_compra")
    private Long id_compra;
    
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToMany( fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "id_registro_venta")
	private List<RegistroVentaProductor> registrosVentaProductor;
	
    
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idRegistroVentaProductor")
	private RegistroVentaProductor seleccionProductor;
	
    public RegistroVenta() {
		
		// TODO Auto-generated constructor stub
    	this.idRegistroVenta = null;
    	this.producto = null;
    	this.cantidad = 0;
    	this.id_compra = null;
    	
    	this.registrosVentaProductor = new ArrayList<RegistroVentaProductor>();
		this.seleccionProductor = null;
	}
    
    public RegistroVenta(Long idRegistroVenta, Producto producto, Integer cantidad,Long id_venta,
    		List<RegistroVentaProductor> registrosVentaProductor, RegistroVentaProductor seleccionProductor
    		) {
		
		this.idRegistroVenta = idRegistroVenta;
		this.producto = producto;
		this.cantidad = cantidad;
		this.id_compra = id_venta;
		
		this.registrosVentaProductor = registrosVentaProductor;
		this.seleccionProductor = seleccionProductor;
	}
	
   

	public Long getIdRegistroVenta() {
		return idRegistroVenta;
	}

	public void setIdRegistroVenta(Long idRegistroVenta) {
		this.idRegistroVenta = idRegistroVenta;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	
    public Long getId_compra() {
		return id_compra;
	}

	public void setId_compra(Long id_venta) {
		this.id_compra = id_venta;
	}
	
	
	

	public List<RegistroVentaProductor> getRegistrosVentaProductor() {
		return registrosVentaProductor;
	}

	public void setRegistrosVentaProductor(List<RegistroVentaProductor> registrosVentaProductor) {
		this.registrosVentaProductor = registrosVentaProductor;
	}

	public RegistroVentaProductor getSeleccionProductor() {
		return seleccionProductor;
	}

	public void setSeleccionProductor(RegistroVentaProductor seleccionProductor) {
		this.seleccionProductor = seleccionProductor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    
    
    
    
}
