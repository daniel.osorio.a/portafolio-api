package com.virtualferia.models.dto;


public class ResumenVentaDTO {

	String Periodo;
	Integer MontoVenta;
	
	public ResumenVentaDTO() {
		 
		// TODO Auto-generated constructor stub
	}
	public ResumenVentaDTO(String periodo, Integer montoVenta) {
		 
		Periodo = periodo;
		MontoVenta = montoVenta;
	}
	public String getPeriodo() {
		return Periodo;
	}
	public void setPeriodo(String periodo) {
		Periodo = periodo;
	}
	public Integer getMontoVenta() {
		return MontoVenta;
	}
	public void setMontoVenta(Integer montoVenta) {
		MontoVenta = montoVenta;
	}
	
	
	
	
}
