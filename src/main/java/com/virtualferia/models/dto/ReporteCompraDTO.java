package com.virtualferia.models.dto;

import java.util.Date;

public class ReporteCompraDTO {
	
	private Long idCompra;
	private Integer costoTotal;
	private Date fechaInicio;
	private Long idComprador;
	
	public ReporteCompraDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ReporteCompraDTO(Long idCompra, Integer costoTotal, Date fechaInicio, Long idComprador) {
		this.idCompra = idCompra;
		this.costoTotal = costoTotal;
		this.fechaInicio = fechaInicio;
		this.idComprador = idComprador;
	}

	public Long getIdComprador() {
		return idComprador;
	}
	public void setIdComprador(Long idComprador) {
		this.idComprador = idComprador;
	}

	public Long getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(Long idCompra) {
		this.idCompra = idCompra;
	}

	public Integer getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(Integer costoTotal) {
		this.costoTotal = costoTotal;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	 
	
	
	
	

}
