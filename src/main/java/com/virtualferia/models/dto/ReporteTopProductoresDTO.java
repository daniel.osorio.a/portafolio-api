package com.virtualferia.models.dto;

public class ReporteTopProductoresDTO {

	Integer idProductor;
	String nombreProductor;
	Integer totalRegistrosDeVenta;
	
	
	
	
	
	public ReporteTopProductoresDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReporteTopProductoresDTO(Integer idProductor, String nombreProductor, Integer totalRegistrosDeVenta) {
		super();
		this.idProductor = idProductor;
		this.nombreProductor = nombreProductor;
		this.totalRegistrosDeVenta = totalRegistrosDeVenta;
	}
	public Integer getIdProductor() {
		return idProductor;
	}
	public void setIdProductor(Integer idProductor) {
		this.idProductor = idProductor;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
	public Integer getTotalRegistrosDeVenta() {
		return totalRegistrosDeVenta;
	}
	public void setTotalRegistrosDeVenta(Integer totalRegistrosDeVenta) {
		this.totalRegistrosDeVenta = totalRegistrosDeVenta;
	}
	
	
	
}
