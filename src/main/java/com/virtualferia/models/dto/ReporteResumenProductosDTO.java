package com.virtualferia.models.dto;

public class ReporteResumenProductosDTO {

	Integer idProducto;
	String nombre;
	Integer cantidadProducto;
	Integer numeroRegistro;
	
	
	public ReporteResumenProductosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReporteResumenProductosDTO(Integer idProducto, String nombre, Integer cantidadProducto,
			Integer numeroRegistro) {
		super();
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.cantidadProducto = cantidadProducto;
		this.numeroRegistro = numeroRegistro;
	}
	
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getCantidadProducto() {
		return cantidadProducto;
	}
	public void setCantidadProducto(Integer cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}
	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	
	
	
			
}
