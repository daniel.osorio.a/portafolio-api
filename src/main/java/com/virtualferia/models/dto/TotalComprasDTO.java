package com.virtualferia.models.dto;

public class TotalComprasDTO {
	
	Integer totalCompras;
	Integer totalComprasMonto;
	
	
	
	
	public TotalComprasDTO() {
		this.totalCompras = 0;
		this.totalComprasMonto = 0;
		// TODO Auto-generated constructor stub
	}
	public TotalComprasDTO(Integer totalCompras, Integer totalComprasMonto) {
		
		this.totalCompras = totalCompras;
		this.totalComprasMonto = totalComprasMonto;
	}
	public Integer getTotalCompras() {
		return totalCompras;
	}
	public void setTotalCompras(Integer totalCompras) {
		this.totalCompras = totalCompras;
	}
	public Integer getTotalComprasMonto() {
		return totalComprasMonto;
	}
	public void setTotalComprasMonto(Integer totalComprasMonto) {
		this.totalComprasMonto = totalComprasMonto;
	}
	
	

}
