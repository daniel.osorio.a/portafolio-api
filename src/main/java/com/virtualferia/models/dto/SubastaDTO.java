package com.virtualferia.models.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import com.virtualferia.models.entity.RegistroSubasta;

public class SubastaDTO {
	

	@Id
    private Long idSubasta;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaInicio;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
    private Date fechaFin;
	
    private List<RegistroSubasta> registros;
    
    private RegistroSubasta SubastaSeleccionada;
    
    private Long compra;
    
    private Long idEstadoCompra;
    
    

	public SubastaDTO(Long idSubasta, Date fechaInicio, Date fechaFin, List<RegistroSubasta> registros,
			RegistroSubasta subastaSeleccionada, Long compra, Long idEstadoCompra) {
		this.idSubasta = idSubasta;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.registros = registros;
		SubastaSeleccionada = subastaSeleccionada;
		this.compra = compra;
		this.idEstadoCompra = idEstadoCompra;
	}
	
	

	public SubastaDTO() {
	}



	public Long getIdSubasta() {
		return idSubasta;
	}

	public void setIdSubasta(Long idSubasta) {
		this.idSubasta = idSubasta;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<RegistroSubasta> getRegistros() {
		return registros;
	}

	public void setRegistros(List<RegistroSubasta> registros) {
		this.registros = registros;
	}

	public RegistroSubasta getSubastaSeleccionada() {
		return SubastaSeleccionada;
	}

	public void setSubastaSeleccionada(RegistroSubasta subastaSeleccionada) {
		SubastaSeleccionada = subastaSeleccionada;
	}

	public Long getCompra() {
		return compra;
	}

	public void setCompra(Long compra) {
		this.compra = compra;
	}

	public Long getIdEstadoCompra() {
		return idEstadoCompra;
	}

	public void setIdEstadoCompra(Long idEstadoCompra) {
		this.idEstadoCompra = idEstadoCompra;
	}
    
    

}
