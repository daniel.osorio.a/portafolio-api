package com.virtualferia.models.dto;

public class ReporteResumenProductoresDTO {


	Integer TotalRegistros;
	Integer TotalProductos;
	
	
	public ReporteResumenProductoresDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReporteResumenProductoresDTO(Integer totalRegistros, Integer totalProductos) {
		super();
		TotalRegistros = totalRegistros;
		TotalProductos = totalProductos;
	}
	public Integer getTotalRegistros() {
		return TotalRegistros;
	}
	public void setTotalRegistros(Integer totalRegistros) {
		TotalRegistros = totalRegistros;
	}
	public Integer getTotalProductos() {
		return TotalProductos;
	}
	public void setTotalProductos(Integer totalProductos) {
		TotalProductos = totalProductos;
	}
	
	
}
