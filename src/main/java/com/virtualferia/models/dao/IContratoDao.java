package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Contrato;

@Repository
public interface IContratoDao extends CrudRepository<Contrato, Long>{
}
