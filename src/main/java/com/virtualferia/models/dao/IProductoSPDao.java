package com.virtualferia.models.dao;

import java.util.List;

import com.virtualferia.models.entity.Producto;

public interface IProductoSPDao {
	
    public List<Producto> findAll();
	
	public void save(Producto producto);
	
	public void update(Producto producto);
	 
	public Producto findOne(Long id);
	
	public void delete (Long id);
}
