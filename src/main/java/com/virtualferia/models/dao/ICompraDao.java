package com.virtualferia.models.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Compra;

@Repository
public interface ICompraDao extends JpaRepository<Compra, Long> {
	
	@Query("select c from Compra c join fetch c.registrosCompra r where c.idCompra = :idCompra ")
	public Compra BuscarPorIdConRegistros(@Param("idCompra")Long idCompra);
	
	@Query("select c from Compra c join fetch c.subasta s where c.idCompra = :idCompra ")
	public Compra BuscarPorIdFetchSubasta(@Param("idCompra")Long idCompra);
	
	@Query("select c from Compra c join fetch c.registrosCompra r ")
	public List<Compra> BuscarTodasConRegistros();
	 
}
