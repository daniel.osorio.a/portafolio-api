package com.virtualferia.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Producto;

@Repository
public interface IProductoDao extends JpaRepository< Producto,Long>{
	
	@Query("select p from Producto p where p.nombre like %?1%")
	public List<Producto> buscarPorNombre(String nombre);
}
