package com.virtualferia.models.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Compra;

@Repository
public interface ICompraSPDao {
	
	 public List<Compra> findAll();

}
