package com.virtualferia.models.dao;

import com.virtualferia.models.entity.Usuario;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioDao extends CrudRepository <Usuario,Long > {
	
	
	@Query("select u from Usuario u where u.nombreUsuario = :nombreUsuario and u.contrasena = :contrasena")
	public Usuario buscarPortUserPass(@Param("nombreUsuario")String nombreUsuario,@Param("contrasena")String contrasena);
	
}
