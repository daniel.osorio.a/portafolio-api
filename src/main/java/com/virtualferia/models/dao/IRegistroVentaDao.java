package com.virtualferia.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.RegistroVenta;

@Repository
public interface IRegistroVentaDao extends CrudRepository< RegistroVenta, Long>{
	
	@Query("select rv from RegistroVenta rv left join fetch rv.registrosVentaProductor rvp where rv.idRegistroVenta = :idRegistroVenta ")
	public RegistroVenta BuscarPorIdConRegistros(@Param("idRegistroVenta")Long idRegistroVenta);
}
