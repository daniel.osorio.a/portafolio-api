package com.virtualferia.models.dao.Subasta;

import java.util.List;

import com.virtualferia.models.dto.SubastaDTO;
import com.virtualferia.models.entity.Subasta;

public interface ISubastaSPDao {
	
	List<Subasta> SubastasGanadasPorUsuarioId(Long IdUsuario);


}
