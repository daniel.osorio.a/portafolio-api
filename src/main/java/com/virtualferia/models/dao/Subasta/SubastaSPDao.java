package com.virtualferia.models.dao.Subasta;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.dto.SubastaDTO;
import com.virtualferia.models.entity.Subasta;

@Repository
public class SubastaSPDao implements ISubastaSPDao {

	
	private EntityManager entityManager;
	
	
	@Autowired
	public SubastaSPDao(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	
	@Override
	public List<Subasta> SubastasGanadasPorUsuarioId(Long IdUsuario) {
		List<Subasta> listaSubasta = new ArrayList<Subasta>();  //null;
		try {
		String queryStringBaseAll = "SELECT s.ID_SUBASTA,s.ID_COMPRA,s.FECHA_FIN,s.FECHA_INICIO,s.ID_REGISTRO_SUBASTA " 
									+ "FROM subastas s "
									+ "INNER JOIN REGISTRO_SUBASTAS rs ON rs.ID_SUBASTA = s.ID_SUBASTA "
									+ "INNER JOIN TRANSPORTES te ON te.ID_TRANSPORTE = rs.ID_TRANSPORTE "
									+ "INNER JOIN TRANSPORTISTAS ta ON ta.ID_TRANSPORTE = te.ID_TRANSPORTE "
									+ "INNER JOIN COMPRAS c ON c.ID_COMPRA = s.ID_COMPRA "
									+ "WHERE ta.ID_USUARIO = "+ IdUsuario.toString() + " "
									+ "AND s.ID_REGISTRO_SUBASTA = rs.ID_REGISTRO_SUBASTA " 
									+ "AND c.ID_ESTADO_VENTA = 5 ";  // ojo con esto todas la subastas ganadas y en estado de Esperando entrega = 5
									;
		    	listaSubasta =  entityManager.createNativeQuery(queryStringBaseAll, Subasta.class ).getResultList();
		    } catch (Exception ex) {
		    	System.out.println(ex.getMessage()); 
		    }
		
		
		return listaSubasta;
		
	
	}
	
	
}
