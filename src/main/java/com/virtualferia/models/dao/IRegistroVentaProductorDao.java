package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.RegistroVentaProductor;

@Repository
public interface IRegistroVentaProductorDao extends CrudRepository< RegistroVentaProductor, Long> {
}
