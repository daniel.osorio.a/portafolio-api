package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Transporte;

@Repository
public interface ITransporteDao extends CrudRepository<Transporte, Long> {
	
	
}
	
