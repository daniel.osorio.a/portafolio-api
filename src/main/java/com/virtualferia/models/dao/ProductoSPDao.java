package com.virtualferia.models.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Producto;

@Repository

public class ProductoSPDao implements IProductoSPDao {

	private EntityManager entityManager;

//	private String outGlosa;
//	private Long outEstado;
//	private Long outIdSalida;

	@Autowired
	public ProductoSPDao(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		
		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_GET_ALL_PRODUCTO");

		// Registrar los parámetros de entrada y salida

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_PC_GET_PRODUCTO", void.class,
				ParameterMode.REF_CURSOR);

		// Configuramos el valor de entrada

		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");

		// Obtenemos el resultado del cursos en una lista
		List<Object[]> results = storedProcedureQuery.getResultList();

		List<Producto> resultado = new ArrayList<Producto>();

		for (Object[] objects : results) {
			Producto produ = new Producto();
			produ.setIdProducto(((BigDecimal) objects[0]).longValue());
			produ.setNombre(((String) objects[1]));
			produ.setPeso(((BigDecimal) objects[2]).doubleValue());
			produ.setVolumen(((BigDecimal) objects[3]).doubleValue());

			resultado.add(produ);

		}


//	   // Recorremos la lista con map y devolvemos un List<BusinessObject>
//		results.stream().map(result -> new Producto(
//	        (Long)    (BigDecimal) result[0]. ,
//		    (String) result[1],
//		    (Double) result[2],
//		    (Double) result[3]
//	    )).collect(Collectors.toList());

//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;

		return resultado;
	}

	@Override
	public void save(Producto producto) {
		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_CREAR_PRODUCTO");

		// Registrar los parámetros de entrada y salida
		storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter("IN_PESO", Double.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter("IN_VOLUMEN", Double.class, ParameterMode.IN);

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ID_SALIDA", Integer.class, ParameterMode.OUT);

		// Configuramos el valor de entrada
		storedProcedureQuery.setParameter("IN_NOMBRE", producto.getNombre());
		storedProcedureQuery.setParameter("IN_PESO", producto.getPeso());
		storedProcedureQuery.setParameter("IN_VOLUMEN", producto.getVolumen());
		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
		Integer outputValue3 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ID_SALIDA");

//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;

	}

	@Override
	public Producto findOne(Long id) {

		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_GET_PRODUCTO");

		// Registrar los parámetros de entrada y salida
		storedProcedureQuery.registerStoredProcedureParameter("IN_ID_PRODUCTO", Long.class, ParameterMode.IN);

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_PC_GET_PRODUCTO", void.class,
				ParameterMode.REF_CURSOR);

		// Configuramos el valor de entrada
		storedProcedureQuery.setParameter("IN_ID_PRODUCTO", id);

		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");

		// Obtenemos el resultado del cursos en una lista
		List<Object[]> results = storedProcedureQuery.getResultList();

		List<Producto> resultado = new ArrayList<Producto>();
		Producto producto = new Producto();

		for (Object[] objects : results) {
			Producto produ = new Producto();
			produ.setIdProducto(((BigDecimal) objects[0]).longValue());
			produ.setNombre(((String) objects[1]));
			produ.setPeso(((BigDecimal) objects[2]).doubleValue());
			produ.setVolumen(((BigDecimal) objects[3]).doubleValue());

			resultado.add(produ);

		}

		producto = resultado.get(0);

//	   // Recorremos la lista con map y devolvemos un List<BusinessObject>
//		results.stream().map(result -> new Producto(
//	        (Long)    (BigDecimal) result[0]. ,
//		    (String) result[1],
//		    (Double) result[2],
//		    (Double) result[3]
//	    )).collect(Collectors.toList());

		return producto;
//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_DEL_PRODUCTO");

		// Registrar los parámetros de entrada y salida
		storedProcedureQuery.registerStoredProcedureParameter("IN_ID_PRODUCTO", Long.class, ParameterMode.IN);

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);

		// Configuramos el valor de entrada
		storedProcedureQuery.setParameter("IN_ID_PRODUCTO", id);
		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");

//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;

	}

	@Override
	public void update(Producto producto) {
		// TODO Auto-generated method stub
		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_UPD_PRODUCTO");

		// Registrar los parámetros de entrada y salida
		storedProcedureQuery.registerStoredProcedureParameter("IN_ID_PRODUCTO", Long.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter("IN_PESO", Double.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter("IN_VOLUMEN", Double.class, ParameterMode.IN);

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);

		// Configuramos el valor de entrada
		storedProcedureQuery.setParameter("IN_ID_PRODUCTO", producto.getIdProducto());
		storedProcedureQuery.setParameter("IN_NOMBRE", producto.getNombre());
		storedProcedureQuery.setParameter("IN_PESO", producto.getPeso());
		storedProcedureQuery.setParameter("IN_VOLUMEN", producto.getVolumen());
		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");

//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;
	}

}
