package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.PagoDistribucion;

@Repository
public interface IPagoDistribucionDao  extends CrudRepository<PagoDistribucion, Long> {

}
