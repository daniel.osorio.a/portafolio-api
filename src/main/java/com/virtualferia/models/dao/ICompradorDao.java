package com.virtualferia.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Comprador;

@Repository
public interface ICompradorDao extends CrudRepository<Comprador, Long> {
	
	@Query("select c from Comprador c where c.usuario.idUsuario = :idUsuario ")
	public Comprador buscarPorUsuarioId(@Param("idUsuario")Long idUsuario);
	
}
