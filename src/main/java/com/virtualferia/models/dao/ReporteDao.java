package com.virtualferia.models.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.dto.ReporteCompraDTO;
import com.virtualferia.models.dto.ReporteResumenProductoresDTO;
import com.virtualferia.models.dto.ReporteResumenProductosDTO;
import com.virtualferia.models.dto.ReporteTopProductoresDTO;
import com.virtualferia.models.dto.ResumenVentaDTO;
import com.virtualferia.models.dto.TotalComprasDTO;

@Repository
public class ReporteDao implements IReporteDao{
	
	private EntityManager entityManager;
	
	
	@Autowired
	public ReporteDao(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}


	@Override
	public TotalComprasDTO TotalCompras() {
		
		TotalComprasDTO totalCompras= new TotalComprasDTO();
		try {
			
			StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("USER_FERIA2.SP_REP_TOTAL_VENTAS");

			// Registrar los parámetros de entrada y salida
			
			// Ejemplor registro Parametro de enterada
			// storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);

			storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_RESULTADO", void.class,ParameterMode.REF_CURSOR);
			
			// Realizamos la llamada al procedimiento
			storedProcedureQuery.execute();

			// Obtenemos los valores de salida
			// String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
			// Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
			
			// Object outputValue3 =  storedProcedureQuery.getOutputParameterValue("OUT_RESULTADO");
			
			// Obtenemos el resultado del cursos en una lista
			List<Object[]> results = storedProcedureQuery.getResultList();
			
			for (Object[] objects : results) {
				totalCompras.setTotalCompras(  		((BigDecimal) objects[0]).intValue()  );
				totalCompras.setTotalComprasMonto(  ((BigDecimal) objects[1]).intValue()   );
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return totalCompras;
		
	}


	@Override
	public List<ReporteCompraDTO> TopTresCompradores() {
		
		List<ReporteCompraDTO> top3 = new ArrayList<ReporteCompraDTO>();
		
		try {
			
			StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("USER_FERIA2.SP_REP_TOP_COMPRADORES");

			// Registrar los parámetros de entrada y salida
			
			// Ejemplor registro Parametro de enterada
			// storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);

			storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_RESULTADO", void.class,ParameterMode.REF_CURSOR);
			
			// Realizamos la llamada al procedimiento
			storedProcedureQuery.execute();

			// Obtenemos los valores de salida
			// String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
			// Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
			
			// Object outputValue3 =  storedProcedureQuery.getOutputParameterValue("OUT_RESULTADO");
			
			// Obtenemos el resultado del cursos en una lista
			List<Object[]> results = storedProcedureQuery.getResultList();
			
			for (Object[] objects : results) {
				ReporteCompraDTO compradorDTO = new ReporteCompraDTO();
				
				compradorDTO.setIdCompra(    ((BigDecimal) objects[0]).longValue()) ;
				compradorDTO.setCostoTotal(  ((BigDecimal) objects[1]).intValue() );
				compradorDTO.setFechaInicio( ((Timestamp) objects[2]) )  ;
				compradorDTO.setIdComprador(  ((BigDecimal) objects[3]).longValue()  );
				top3.add(compradorDTO);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return top3;
	}


	@Override
	public List<ResumenVentaDTO> ResumenAnualVentas() {
		return null;
	}


	@Override
	public ReporteResumenProductoresDTO ResumenRegVenta() {

		ReporteResumenProductoresDTO resumenProductores= new ReporteResumenProductoresDTO();
		try {
			
			StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("USER_FERIA2.SP_REP_RESUMEN_PRODUCTORES");

			// Registrar los parámetros de entrada y salida
			
			// Ejemplor registro Parametro de enterada
			// storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);

			storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_RESULTADO", void.class,ParameterMode.REF_CURSOR);
			
			// Realizamos la llamada al procedimiento
			storedProcedureQuery.execute();

			// Obtenemos los valores de salida
			// String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
			// Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
			
			// Object outputValue3 =  storedProcedureQuery.getOutputParameterValue("OUT_RESULTADO");
			
			// Obtenemos el resultado del cursos en una lista
			List<Object[]> results = storedProcedureQuery.getResultList();
			
			for (Object[] objects : results) {
				resumenProductores.setTotalProductos(  ((BigDecimal) objects[0]).intValue()  );
				resumenProductores.setTotalRegistros(  ((BigDecimal) objects[1]).intValue()   );
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return resumenProductores;
	}


	@Override
	public List<ReporteTopProductoresDTO> TopProductores() {
		List<ReporteTopProductoresDTO> top = new ArrayList<ReporteTopProductoresDTO>();
		try {

			StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("USER_FERIA2.SP_REP_TOP_PRODUCTORES");

			// Registrar los parámetros de entrada y salida
			
			// Ejemplor registro Parametro de enterada
			// storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);

			storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_RESULTADO", void.class,ParameterMode.REF_CURSOR);
			
			// Realizamos la llamada al procedimiento
			storedProcedureQuery.execute();

			// Obtenemos los valores de salida
			// String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
			// Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
			
			// Object outputValue3 =  storedProcedureQuery.getOutputParameterValue("OUT_RESULTADO");
			
			// Obtenemos el resultado del cursos en una lista
			List<Object[]> results = storedProcedureQuery.getResultList();
			
			for (Object[] objects : results) {
				ReporteTopProductoresDTO repProductores = new ReporteTopProductoresDTO();
				
				repProductores.setIdProductor(   ((BigDecimal) objects[0]).intValue()) ;
				repProductores.setNombreProductor(  ((String) objects[1]).toString() );
				repProductores.setTotalRegistrosDeVenta(  ((BigDecimal) objects[2]).intValue()  );
				top.add(repProductores);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return top;
	}


	@Override
	public List<ReporteResumenProductosDTO> ResumenProductos() {
		List<ReporteResumenProductosDTO> top = new ArrayList<ReporteResumenProductosDTO>();
		try {

			StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("USER_FERIA2.SP_REP_RESUMEN_PRODUCTOS");

			// Registrar los parámetros de entrada y salida
			
			// Ejemplor registro Parametro de enterada
			// storedProcedureQuery.registerStoredProcedureParameter("IN_NOMBRE", String.class, ParameterMode.IN);

			storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
			storedProcedureQuery.registerStoredProcedureParameter("OUT_RESULTADO", void.class,ParameterMode.REF_CURSOR);
			
			// Realizamos la llamada al procedimiento
			storedProcedureQuery.execute();

			// Obtenemos los valores de salida
			// String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
			// Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");
			
			// Object outputValue3 =  storedProcedureQuery.getOutputParameterValue("OUT_RESULTADO");
			
			// Obtenemos el resultado del cursos en una lista
			List<Object[]> results = storedProcedureQuery.getResultList();
			
			for (Object[] objects : results) {
				ReporteResumenProductosDTO repProductos = new ReporteResumenProductosDTO();
				
				repProductos.setIdProducto(   ((BigDecimal) objects[0]).intValue()) ;
				repProductos.setNombre(  ((String) objects[1]).toString() );
				repProductos.setCantidadProducto(  ((BigDecimal) objects[2]).intValue()  );
				repProductos.setNumeroRegistro(  ((BigDecimal) objects[3]).intValue()  );
				top.add(repProductos);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return top;
	}

}
