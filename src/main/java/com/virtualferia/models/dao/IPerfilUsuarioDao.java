package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.PerfilUsuario;

@Repository
public interface IPerfilUsuarioDao extends CrudRepository<PerfilUsuario, Long> {
}
