package com.virtualferia.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Productor;
import com.virtualferia.models.entity.Transportista;

@Repository
public interface ITransportistaDao extends CrudRepository<Transportista, Long>{
	
	@Query("select t from Transportista t where t.usuario.idUsuario = :idUsuario ")
	public Transportista buscarPorUsuarioId(@Param("idUsuario")Long idUsuario);

	@Query("select t from Transportista t   where t.transporte.idTransporte = :IdTransportista ")
	public Transportista BuscarTransportistaPorIdTransporte(@Param("IdTransportista")Long IdTransportista);
}
