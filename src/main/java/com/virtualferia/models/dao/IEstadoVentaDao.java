package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.EstadoVenta;

@Repository
public interface IEstadoVentaDao extends CrudRepository<EstadoVenta, Long>{
}
