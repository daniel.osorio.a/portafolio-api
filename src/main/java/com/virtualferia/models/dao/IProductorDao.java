package com.virtualferia.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.virtualferia.models.entity.Productor;

@Repository
public interface IProductorDao extends CrudRepository<Productor, Long> {
	
	@Query("select p from Productor p where p.usuario.idUsuario = :idUsuario ")
	public Productor buscarPorUsuarioId(@Param("idUsuario")Long idUsuario);
}
