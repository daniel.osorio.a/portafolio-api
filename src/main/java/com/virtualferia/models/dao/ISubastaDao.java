package com.virtualferia.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Subasta;

@Repository
public interface ISubastaDao extends CrudRepository<Subasta, Long>{
	
	@Query("select s from Subasta s ")
	public List<Subasta> listarSoloSubastas();
	
	@Query("select s from Subasta s join fetch s.registros r")
	public List<Subasta> SubastaConRegistros();
	
	@Query("select s from Subasta s join fetch s.registros r where s.idSubasta = :idSubasta ")
	public Subasta SubastaConRegistrosPorId(@Param("idSubasta")Long idSubasta);
	
	
}
