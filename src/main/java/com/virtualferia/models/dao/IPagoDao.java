package com.virtualferia.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.Pago;

@Repository
public interface IPagoDao  extends CrudRepository<Pago, Long>{
	
	@Query("select p from Pago p join fetch p.Pagodistribucion pd where p.idPago = :idPago ")
	public Pago BuscarPorIdPagoConRegistros(@Param("idPago")Long idPago);
	
	@Query("select p from Pago p   where p.compra.idCompra = :idCompra ")
	public Pago BuscarPorIdCompra(@Param("idCompra")Long idCompra);
	
	
	@Query("select p from Pago p  join fetch p.Pagodistribucion pd where p.compra.idCompra = :idCompra")
	public Pago BuscarPorIdCompraConRegistros(@Param("idCompra")Long idCompra);
	
}
