package com.virtualferia.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.virtualferia.models.entity.RegistroSubasta;

@Repository
public interface IRegistroSubastaDao extends CrudRepository<RegistroSubasta, Long>{
}
