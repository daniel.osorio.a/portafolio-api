package com.virtualferia.models.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;

import com.virtualferia.models.entity.Compra;

public class CompraSPDao implements ICompraSPDao {
	
	
	private EntityManager entityManager;
	
	@Autowired
	public CompraSPDao(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}


	
	
	@Override
	public List<Compra> findAll() {
		// TODO Auto-generated method stub
		
		StoredProcedureQuery storedProcedureQuery = entityManager
				.createStoredProcedureQuery("USER_FERIA2.SP_GET_ALL_PRODUCTO");

		// Registrar los parámetros de entrada y salida

		storedProcedureQuery.registerStoredProcedureParameter("OUT_GLOSA", String.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_ESTADO", Integer.class, ParameterMode.OUT);
		storedProcedureQuery.registerStoredProcedureParameter("OUT_PC_GET_PRODUCTO", void.class,
				ParameterMode.REF_CURSOR);

		// Configuramos el valor de entrada

		// Realizamos la llamada al procedimiento
		storedProcedureQuery.execute();

		// Obtenemos los valores de salida
		String outputValue1 = (String) storedProcedureQuery.getOutputParameterValue("OUT_GLOSA");
		Integer outputValue2 = (Integer) storedProcedureQuery.getOutputParameterValue("OUT_ESTADO");

		// Obtenemos el resultado del cursos en una lista
		List<Object[]> results = storedProcedureQuery.getResultList();

		List<Compra> resultado = new ArrayList<Compra>();

		for (Object[] objects : results) {
			Compra venta = new Compra();
			
			venta.setIdCompra(((BigDecimal) objects[0]).longValue());
			venta.setCostoTotal(((BigDecimal) objects[1]).doubleValue());
			venta.setFechaInicio((Date)objects[1]);
			//venta.setComprador(comprador);
			
//			venta.setIdProducto(((BigDecimal) objects[0]).longValue());
//			venta.setNombre(((String) objects[1]));
//			venta.setPeso(((BigDecimal) objects[2]).doubleValue());
//			venta.setVolumen(((BigDecimal) objects[3]).doubleValue());

			resultado.add(venta);

		}


//	   // Recorremos la lista con map y devolvemos un List<BusinessObject>
//		results.stream().map(result -> new Producto(
//	        (Long)    (BigDecimal) result[0]. ,
//		    (String) result[1],
//		    (Double) result[2],
//		    (Double) result[3]
//	    )).collect(Collectors.toList());

//        outGlosa = outputValue1;
//    	  outEstado = outputValue2;
//    	  outIdSalida = outputValue3;

		return resultado;
	}

}
