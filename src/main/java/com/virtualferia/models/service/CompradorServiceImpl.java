package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.virtualferia.models.dao.ICompradorDao;
import com.virtualferia.models.entity.Comprador;
import com.virtualferia.models.entity.Productor;


@Service
public class CompradorServiceImpl implements ICompradorService{
	
	@Autowired
	ICompradorDao compradorDao;

	@Override
	@Transactional(readOnly=true)
	public List<Comprador> listar() {
		// TODO Auto-generated method stub
		return (List<Comprador>) compradorDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Comprador cliente) {
		// TODO Auto-generated method stub
		compradorDao.save(cliente);
		
	}

	@Override
	@Transactional(readOnly=true)
	public Comprador buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return compradorDao.findById(id).orElse(null) ;
		  
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		compradorDao.deleteById(id);
		// TODO Auto-generated method stub
		
	}

	@Override
	public Comprador buscarPorUsuarioId(Long idUsuario) {
		// TODO Auto-generated method stub
		return compradorDao.buscarPorUsuarioId(idUsuario);
	}

}
