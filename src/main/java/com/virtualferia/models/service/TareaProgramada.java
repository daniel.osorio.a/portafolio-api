package com.virtualferia.models.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class TareaProgramada {
	
	@Autowired
	IVentaService ventaService;
	 

	@Scheduled(fixedDelay=30000)
    public void ejecutarCadaXSegundos() {
		//Valida Esperando Productores
        System.out.println("Probando... Esperando Productores");
        ventaService.ValidaEstadoEsperandoProductores();
    } 
	
	
	@Scheduled(fixedDelay=60000)
    public void ejecutarCadaXSegundos2() {
		//Valida Esperando Transportista
        System.out.println("Probando... Esperando Transportistas");
        ventaService.ValidaEstadoEsperandoTransportista();
    }
	
}