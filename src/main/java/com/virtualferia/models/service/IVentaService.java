package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Compra;


public interface IVentaService {

	
	public List<Compra> listar();
	
	
	public void guardar(Compra cliente);
	 
	public Compra buscarPorId(Long id);
	
	public void eliminar (Long id);
	
	public void ValidaEstadoEsperandoProductores(Long idRegistroVenta);
	public void ValidaEstadoEsperandoProductores();
	
	public void ValidaEstadoEsperandoTransportista();
	
	public void ValidaEstadoEsperandoPago(Long IdCompra);
	
	public boolean CompletarPedido(Long idSubasta);

	
}
