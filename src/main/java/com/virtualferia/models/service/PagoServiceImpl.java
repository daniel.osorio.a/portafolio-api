package com.virtualferia.models.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.ICompraDao;
import com.virtualferia.models.dao.IPagoDao;
import com.virtualferia.models.dao.IPagoDistribucionDao;
import com.virtualferia.models.dao.IProductorDao;
import com.virtualferia.models.dao.ITransporteDao;
import com.virtualferia.models.dao.ITransportistaDao;
import com.virtualferia.models.entity.Compra;
import com.virtualferia.models.entity.Pago;
import com.virtualferia.models.entity.PagoDistribucion;
import com.virtualferia.models.entity.Productor;
import com.virtualferia.models.entity.RegistroSubasta;
import com.virtualferia.models.entity.RegistroVenta;
import com.virtualferia.models.entity.RegistroVentaProductor;
import com.virtualferia.models.entity.Subasta;
import com.virtualferia.models.entity.Transporte;
import com.virtualferia.models.entity.Transportista;

@Service
public class PagoServiceImpl implements IPagoService {

	
	@Autowired
	IPagoDao pagoDao;
	
	@Autowired
	ITransporteDao transporteDao;
	
	@Autowired
	ITransportistaDao transportistaDao;
	
	@Autowired
	IPagoDistribucionDao pagoDistribucionDao;
	
	@Autowired
	ICompraDao compraDao;
	
	@Autowired
	IProductorDao productorDao;
	
	@Autowired
	IVentaService ventaService;
	
	@Override
	public List<Pago> listar() {
		// TODO Auto-generated method stub
		return (List<Pago>)pagoDao.findAll();
	}

	@Override
	public void guardar(Pago pago) {
		// TODO Auto-generated method stub
		pagoDao.save(pago);
		
	}

	@Override
	public Pago buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return pagoDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		pagoDao.deleteById(id);
		
	}

	@Override
	public boolean pagar(Pago pago) {
		boolean resultado = false;
		
		try {
			// Buscar Pago por compra
			Pago pagoBuscado = pagoDao.BuscarPorIdCompra(pago.getCompra().getIdCompra());
			
			if (!pagoBuscado.isPagado()) {
				// Modificar Fecha Pago Cambiar Pagado a True
				Calendar calendar = Calendar.getInstance();
				
				pagoBuscado.setFechaPago(calendar.getTime());
				
				// Distribuir Pago
				DistribuirPago(pagoBuscado);
				
				pagoBuscado.setPagado(true);
				
				pagoDao.save(pagoBuscado);
				
				resultado = true;
				
				ventaService.ValidaEstadoEsperandoPago(pago.getCompra().getIdCompra());
			}
			
			
			
			 
				
			
			
		} catch (Exception e) {
			
		}
		
		
		return resultado;
	}

	@Override
	public Pago buscarPorIdCompra(Long idCompra) {
		// TODO Auto-generated method stub
		return pagoDao.BuscarPorIdCompra(idCompra);
	}
	
	
	private void DistribuirPago(Pago pago) {
		
		Compra compra = compraDao.BuscarPorIdFetchSubasta(pago.getCompra().getIdCompra());
		
		
		//Compra compra = new Compra();
		
		
		
		// Distribuir Pago Transporte
		Subasta subasta = compra.getSubasta();
		
		// - Buscar Ganadaor Subasta  = subasta.SubastaSeleccionada
		RegistroSubasta registroSubasta = subasta.getSubastaSeleccionada();

		// - Buscar el Precio del transporte transporte
		Transporte transporte = transporteDao.findById(registroSubasta.getId_transporte()).orElse(null);
		
		// - Buscar usuario del transporte 
		Transportista transportista = transportistaDao.BuscarTransportistaPorIdTransporte(transporte.getIdTransporte());
		
		// - Ingresar Registro en Distribucion Pago
		
		PagoDistribucion pagoDistribucion = new PagoDistribucion();
		pagoDistribucion.setId_pago(pago.getIdPago());
		Calendar calendar = Calendar.getInstance();
		pagoDistribucion.setFechaRegistro(calendar.getTime());
		pagoDistribucion.setUsuario( transportista.getUsuario().getIdUsuario());
		pagoDistribucion.setMonto(transporte.getPrecio());
		
		pagoDistribucionDao.save(pagoDistribucion);
		
		// Distribuir Pago Productores
		
		compra = compraDao.BuscarPorIdConRegistros(compra.getIdCompra());
		
		for (RegistroVenta regVenta : compra.getRegistrosCompra()) {
			
			RegistroVentaProductor regVentaProductor =  regVenta.getSeleccionProductor();
			
			PagoDistribucion pagoDistribucion2 = new PagoDistribucion();
			pagoDistribucion2.setId_pago(pago.getIdPago());
			pagoDistribucion2.setFechaRegistro(calendar.getTime());
			
			Productor productor = productorDao.findById(regVentaProductor.getId_productor()).orElse(null);
			pagoDistribucion2.setUsuario(productor.getUsuario().getIdUsuario());
			pagoDistribucion2.setMonto(regVentaProductor.getPrecio());
			
			pagoDistribucionDao.save(pagoDistribucion2);
			
			
		}
		
		
		// Recorrer Registro Venta 
		
		// Traer Registro Venta Productor
		
		// Buscar Detalle Venta 
	}

}
