package com.virtualferia.models.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.ICompraDao;
import com.virtualferia.models.dao.ICompradorDao;
import com.virtualferia.models.dao.IEstadoVentaDao;
import com.virtualferia.models.dao.IPagoDao;
import com.virtualferia.models.dao.IProductorDao;
import com.virtualferia.models.dao.IRegistroVentaDao;
import com.virtualferia.models.dao.ISubastaDao;
import com.virtualferia.models.dao.ITransporteDao;
import com.virtualferia.models.dao.ITransportistaDao;
import com.virtualferia.models.dao.IUsuarioDao;
import com.virtualferia.models.entity.Compra;
import com.virtualferia.models.entity.Comprador;
import com.virtualferia.models.entity.EstadoVenta;
import com.virtualferia.models.entity.Pago;
import com.virtualferia.models.entity.Productor;
import com.virtualferia.models.entity.RegistroSubasta;
import com.virtualferia.models.entity.RegistroVenta;
import com.virtualferia.models.entity.RegistroVentaProductor;
import com.virtualferia.models.entity.Subasta;
import com.virtualferia.models.entity.Transporte;
import com.virtualferia.models.entity.Transportista;
import com.virtualferia.models.entity.Usuario;

@Service
public class VentaServiceImpl implements IVentaService {
	
	@Autowired
	ICompraDao ventaDao;
	
	@Autowired
	ISubastaDao subastaDao;
	
	@Autowired
	IRegistroVentaDao registroVentaDao;
	
	@Autowired
	ITransporteDao transporteDao;
	
	@Autowired
	IPagoDao pagoDao;
	
	@Autowired
	IProductorDao productorDao;
	
	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	ICompradorDao compradorDao;
	
	@Autowired
	ITransportistaDao transportistaDao;
	
	@Autowired
	IEstadoVentaDao estadoVentaDao;
	
	@Autowired
	EnvioEmail envioEmail;
	

	
	@Override
	public List<Compra> listar() {
		List<Compra> resultado = new ArrayList<Compra>();
		try {
			resultado =  (List<Compra>) ventaDao.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		// TODO Auto-generated method stub
		return resultado;
	}

	@Override
	public void guardar(Compra compra) {
		
		boolean crearSubasta = false;
		// Si es una nueva compra crea subasta
		if(compra.getIdCompra() == null) {
			//CrearSubasta();
			crearSubasta = true;
		}	
		
		ventaDao.save(compra);
		
		// Crea Subasta al mismo tiempo que la compra
		if (crearSubasta) {
			Subasta subasta = CrearSubasta(compra);
			compra.setSubasta(subasta);
			ventaDao.save(compra);
		}
		 	
			
	}

	@Override
	public Compra buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return ventaDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		ventaDao.deleteById(id);
	}
	
	public Subasta CrearSubasta(Compra compra) {
		Subasta subasta = new Subasta();
		
		Calendar calendar = Calendar.getInstance();
		LocalDate.now();
		
		subasta.setFechaInicio(calendar.getTime());
		calendar.add(calendar.DAY_OF_MONTH ,+3);
		subasta.setFechaFin(calendar.getTime());
		
		
		subasta.setCompra(compra.getIdCompra());
		
		subastaDao.save(subasta);
		
		return subasta;
		
	}
	
	@Override
	public void ValidaEstadoEsperandoProductores(Long idRegistroVenta) {
		try {
			RegistroVenta regVenta = registroVentaDao.findById(idRegistroVenta).orElse(null);
			System.out.println("regventa");
			System.out.println(regVenta);
			System.out.println(regVenta.getCantidad());
			
			Compra compra = ventaDao.findById(regVenta.getId_compra()).orElse(null);
			System.out.println("compra");
			System.out.println(compra);
			System.out.println(compra.getEstadoVenta().getNombre());
			
			// verifica si todos los registros cuentan con un registro productor
			boolean cambiar = true;
			
			
			
			for ( RegistroVenta registros : compra.getRegistrosCompra()) {
				
				if(registros.getRegistrosVentaProductor().size() == 0) {
					cambiar = false;
				}
				
			}
			
			// si todos los registros cuentan con un registro productor cambia la venta a estado 3 (Esperando Transporte)
			if (cambiar) {
				
				EstadoVenta estadoVenta = new EstadoVenta();
				estadoVenta.setIdEstadoVenta(new Long(3));
				compra.setEstadoVenta(estadoVenta);
				
				// Seleccionar Mejores Productores 
				List<RegistroVenta> RegistrosCompra = compra.getRegistrosCompra();
				for (int i = 0; i < RegistrosCompra.size(); i++) {
					
					RegistroVenta registro = RegistrosCompra.get(i);
					registro.setSeleccionProductor(MejorProductor(registro.getIdRegistroVenta()));
					
					RegistrosCompra.set(i, registro);
				}
				
				// Actualiza Costo total Compra con valor de los Productores
				Double TotalValorCompra = 0.0;
				for ( RegistroVenta registros : RegistrosCompra ) {

					TotalValorCompra = TotalValorCompra + registros.getSeleccionProductor().getPrecio();
				}
				compra.setCostoTotal(TotalValorCompra);
				ventaDao.save(compra);
				
				
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error");
		}
		
	}
	
	@Override
	public void ValidaEstadoEsperandoProductores() {
		try {
			
			for (Compra compraItem : ventaDao.BuscarTodasConRegistros()) {
				Compra compra = compraItem;
				
				// Si el estado de la venta es menor a 3 Esperando (Transportista) valida si pasa
				if(compra.getEstadoVenta().getIdEstadoVenta() < 3 ){
					
					// verifica si todos los registros cuentan con un registro productor
					boolean cambiar = true;
					
					if (compra.getRegistrosCompra().size() >= 1) {
						for ( RegistroVenta registro : compra.getRegistrosCompra()) {
							
							RegistroVenta registro2 = registroVentaDao.BuscarPorIdConRegistros(registro.getIdRegistroVenta());
							
							if(registro2 != null) {
								
								registro.setRegistrosVentaProductor(registro2.getRegistrosVentaProductor());
							
								if(registro.getRegistrosVentaProductor().size() == 0 ) {
									cambiar = false;
								}
							}
							
							
						}
					}else {
						cambiar = false;
					}
					
					
					// si todos los registros cuentan con un registro productor cambia la venta a estado 3 (Esperando Transporte)
					if (cambiar) {
						EstadoVenta estadoVenta = new EstadoVenta();
						estadoVenta.setIdEstadoVenta(new Long(3));
						compra.setEstadoVenta(estadoVenta);
						
						// Seleccionar Mejores Productores 
						List<RegistroVenta> RegistrosCompra = compra.getRegistrosCompra();
						for (int i = 0; i < RegistrosCompra.size(); i++) {
							
							RegistroVenta registro = RegistrosCompra.get(i);
							// registro.setSeleccionProductor(MejorProductor(registro.getIdRegistroVenta()));
							registro.setSeleccionProductor(MejorProductor(registro));
							
							RegistrosCompra.set(i, registro);
							
							// Envio de correo a Productor
							Long id_productor = registro.getSeleccionProductor().getId_productor();
							Productor productor = productorDao.findById(id_productor).orElse(null); 
							// usuarioDao.findById(productor.getUsuario().getIdUsuario()).orElse(null);
							Usuario usuarioProductor = productor.getUsuario(); 
							envioEmail.sendEmail(
									usuarioProductor.getEmail(),
									" Seleccion productor ",
									" Estimado : " + usuarioProductor.getNombreUsuario().toUpperCase() +
									" El registro numero : " + registro.getSeleccionProductor().getIdRegistroVentaProductor().toString() +
									" Ha sido seleccionado "
									);
							
						}
						
						// Actualiza Costo total Compra con valor de los Productores
						Double TotalValorCompra = 0.0;
						for ( RegistroVenta registros : RegistrosCompra ) {

							TotalValorCompra = TotalValorCompra + registros.getSeleccionProductor().getPrecio();
						}
						compra.setCostoTotal(TotalValorCompra);
						ventaDao.save(compra);
						
						// Envio Mail a Comprador
						Long idComprador = compra.getId_comprador(); 
						Comprador comprador = compradorDao.findById(idComprador).orElse(null);
						Usuario usuarioComprador = comprador.getUsuario();
						EstadoVenta estadoVenta2 = estadoVentaDao.findById(compra.getEstadoVenta().getIdEstadoVenta()).orElse(null);
						envioEmail.sendEmail(
								usuarioComprador.getEmail(),
								" Compra Actualizacion estado ",
								" Estimado : " + usuarioComprador.getNombreUsuario().toUpperCase() +
								" El Numero de Compra : " + compra.getIdCompra().toString() +
								" Ha sido Actualizada a : "+ estadoVenta2.getNombre() //compra.getEstadoVenta().getNombre().toUpperCase()
								);
						
					}
					
				}
				 
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error");
			System.out.println(e.getMessage());
		}
		
	}
	
	
	public RegistroVentaProductor MejorProductor(Long IdRegistroVenta) {
		RegistroVenta regVenta = registroVentaDao.findById(IdRegistroVenta).orElse(null);
		
		if (regVenta == null) {
			return null;
		}
		RegistroVentaProductor resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroVentaProductor registroVentaProductor : regVenta.getRegistrosVentaProductor()) {
			
			if (registroVentaProductor.getPrecio() <= menorPrecio || resultadoMenorPrecio == null) {
				menorPrecio = registroVentaProductor.getPrecio();
				resultadoMenorPrecio = registroVentaProductor;
			}
			
		}
		// Actualiza registro en BD
		regVenta.setSeleccionProductor(resultadoMenorPrecio);
		registroVentaDao.save(regVenta);
		
		return resultadoMenorPrecio;
	}
	
	public RegistroVentaProductor MejorProductor(RegistroVenta regVenta) {
		 
		if (regVenta == null) {
			return null;
		}
		RegistroVentaProductor resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroVentaProductor registroVentaProductor : regVenta.getRegistrosVentaProductor()) {
			
			if (registroVentaProductor.getPrecio() <= menorPrecio || resultadoMenorPrecio == null) {
				menorPrecio = registroVentaProductor.getPrecio();
				resultadoMenorPrecio = registroVentaProductor;
			}
			
		}
		// Actualiza registro en BD
		regVenta.setSeleccionProductor(resultadoMenorPrecio);
		registroVentaDao.save(regVenta);
		
		return resultadoMenorPrecio;
	}
	
	@Override
	public void ValidaEstadoEsperandoTransportista() {
		
		for (Compra compraItem : ventaDao.BuscarTodasConRegistros()) {
			Compra compra = compraItem;
			
			
			// Si el estado de la venta es menor a 3 Esperando (Transportista) valida si pasa
			if(compra.getEstadoVenta().getIdEstadoVenta() == 3 ){
				
				// verifica si la subasta cuenta con al menos un registro subasta
				boolean cambiar = true;
				
				Subasta subasta = compra.getSubasta();
				
				subasta = subastaDao.SubastaConRegistrosPorId(compra.getSubasta().getIdSubasta());
				if( subasta.getRegistros().size() == 0) {
					cambiar = false;
				}

				
				// si todos los registros cuentan con un registro subasta cambia la venta a estado 4 (Esperando Pago)
				if (cambiar) {
					EstadoVenta estadoVenta = new EstadoVenta();
					estadoVenta.setIdEstadoVenta(new Long(4));
					compra.setEstadoVenta(estadoVenta);
					
					Double TotalValorCompra = compra.getCostoTotal();
					// Seleccionar Mejores Transportista 
					RegistroSubasta regSubasta = MejorRegistroSubasta(subasta.getIdSubasta());
					
					Transporte transporte = transporteDao.findById(regSubasta.getId_transporte()).orElse(null);
					// Actualiza Costo total Compra con valor de los Productores
					TotalValorCompra = TotalValorCompra + transporte.getPrecio();
					
					compra.setCostoTotal(TotalValorCompra);
					ventaDao.save(compra);
					
					// Crear Registro de Pago
					Pago pago = new Pago();
					pago.setCompra(compra);
					Calendar calendar = Calendar.getInstance();
					pago.setFechaRegistro(calendar.getTime());
					pago.setMonto(TotalValorCompra);
					pago.setPagado(false);
					
					pagoDao.save(pago);
					
					// Envio Mail a Transportista
					Transportista transportista = transportistaDao.BuscarTransportistaPorIdTransporte(regSubasta.getId_transporte());
					Usuario usuarioTransportista = transportista.getUsuario();
					envioEmail.sendEmail(
							usuarioTransportista.getEmail(),
							" Seleccion Subasta " + regSubasta.getId_subasta().toString(),
							" Estimado : " + usuarioTransportista.getNombreUsuario().toUpperCase() +
							" El Numero de registro : " + regSubasta.getIdRegistroSubasta().toString() +
							" Ha sido seleccionado   " 
							);
					
					// Envio Mail a Comprador
					Long idComprador = compra.getId_comprador(); 
					Comprador comprador = compradorDao.findById(idComprador).orElse(null);
					Usuario usuarioComprador = comprador.getUsuario();
					EstadoVenta estadoVenta2 = estadoVentaDao.findById(compra.getEstadoVenta().getIdEstadoVenta()).orElse(null);
					envioEmail.sendEmail(
							usuarioComprador.getEmail(),
							" Compra Actualizacion estado ",
							" Estimado : " + usuarioComprador.getNombreUsuario().toUpperCase() +
							" El Numero de Compra : " + compra.getIdCompra().toString() +
							" Ha sido Actualizada a : "+ estadoVenta2.getNombre().toUpperCase()
							);
				}
				
			}
			
			
		}
		
	}
	
	public RegistroSubasta MejorRegistroSubasta(Long IdSubasta) {
		
		Subasta subasta = subastaDao.SubastaConRegistrosPorId(IdSubasta);
		
		RegistroSubasta resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroSubasta registroSubasta : subasta.getRegistros()) {
			
			
			Transporte transporte = transporteDao.findById(registroSubasta.getId_transporte()).orElse(null);
			
			if (transporte.getPrecio() <= menorPrecio || resultadoMenorPrecio == null) {
				menorPrecio = transporte.getPrecio();
				resultadoMenorPrecio = registroSubasta;
			}
			
		}
		
		subasta.setSubastaSeleccionada(resultadoMenorPrecio);
		subastaDao.save(subasta);
		
		return resultadoMenorPrecio;
		
	}
	
	@Override
	public void ValidaEstadoEsperandoPago(Long IdCompra) {
		
		Compra compra = ventaDao.findById(IdCompra).orElse(null);
		
		EstadoVenta estadoVenta = new EstadoVenta();
		estadoVenta.setIdEstadoVenta(new Long(5));
		compra.setEstadoVenta(estadoVenta);
		
		ventaDao.save(compra);
		
		// Envio Mail a Comprador
		Long idComprador = compra.getId_comprador(); 
		Comprador comprador = compradorDao.findById(idComprador).orElse(null);
		Usuario usuarioComprador = comprador.getUsuario();
		EstadoVenta estadoVenta2 = estadoVentaDao.findById(compra.getEstadoVenta().getIdEstadoVenta()).orElse(null);
		envioEmail.sendEmail(
				usuarioComprador.getEmail(),
				" Compra Actualizacion estado ",
				" Estimado : " + usuarioComprador.getNombreUsuario().toUpperCase() +
				" El Numero de Compra : " + compra.getIdCompra().toString() +
				" Ha sido Actualizada a : "+ estadoVenta2.getNombre().toUpperCase()
				);
	}

	@Override
	public boolean CompletarPedido(Long idSubasta) {
		
		Subasta subasta = subastaDao.findById(idSubasta).orElse(null);
		
		if (subasta != null) {
			
			Compra compra = ventaDao.findById(subasta.getCompra()).orElse(null);
			
			EstadoVenta estadoVenta = new EstadoVenta();
			estadoVenta.setIdEstadoVenta(new Long(6));
			compra.setEstadoVenta(estadoVenta);
			
			ventaDao.save(compra);
			
			// Envio Mail a Comprador
			Long idComprador = compra.getId_comprador(); 
			Comprador comprador = compradorDao.findById(idComprador).orElse(null);
			Usuario usuarioComprador = comprador.getUsuario();
			EstadoVenta estadoVenta2 = estadoVentaDao.findById(compra.getEstadoVenta().getIdEstadoVenta()).orElse(null);
			envioEmail.sendEmail(
					usuarioComprador.getEmail(),
					" Compra Actualizacion estado ",
					" Estimado : " + usuarioComprador.getNombreUsuario().toUpperCase() +
					" El Numero de Compra : " + compra.getIdCompra().toString() +
					" Ha sido Actualizada a : "+ estadoVenta2.getNombre().toUpperCase()
					);
			
		}
		
		return false;
	}
	
	
	


}
