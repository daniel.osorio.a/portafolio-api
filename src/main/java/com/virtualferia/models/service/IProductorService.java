package com.virtualferia.models.service;

import java.util.List;


import com.virtualferia.models.entity.Productor;


public interface IProductorService {
	
	public List<Productor> listar();
	
	public void guardar(Productor productor);
	 
	public Productor buscar(Long id);
	
	public Productor buscarPorUsuarioId(Long id);
	
	public void eliminar (Long id);
	
	public void Actualizar(Productor productor); 

}
