package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IRegistroVentaDao;
import com.virtualferia.models.entity.RegistroVenta;
import com.virtualferia.models.entity.RegistroVentaProductor;

@Service
public class RegistroVentaServiceImpl implements IRegistroVentaService{
	
	@Autowired
	IRegistroVentaDao registroventaDao;

	@Override
	public List<RegistroVenta> listar() {
		// TODO Auto-generated method stub
		return (List<RegistroVenta>)registroventaDao.findAll();
	}

	@Override
	public void guardar(RegistroVenta registroVenta) {
		// TODO Auto-generated method stub
		registroventaDao.save(registroVenta);
	}

	@Override
	public RegistroVenta buscar(Long id) {
		// TODO Auto-generated method stub
		return registroventaDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		registroventaDao.deleteById(id);
		
	}

	@Override
	public void ActualizarSP(RegistroVenta registroVenta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RegistroVentaProductor MejorProductor(Long IdRegistroVenta) {
		RegistroVenta regVenta = registroventaDao.findById(IdRegistroVenta).orElse(null);
		
		if (regVenta == null) {
			return null;
		}
		RegistroVentaProductor resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroVentaProductor registroVentaProductor : regVenta.getRegistrosVentaProductor()) {
			
			if (registroVentaProductor.getPrecio() <= menorPrecio) {
				menorPrecio = registroVentaProductor.getPrecio();
				resultadoMenorPrecio = registroVentaProductor;
			}
			
		}
		
		
		return resultadoMenorPrecio;
	}

}
