package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IReporteDao;
import com.virtualferia.models.dto.ReporteCompraDTO;
import com.virtualferia.models.dto.ReporteResumenProductoresDTO;
import com.virtualferia.models.dto.ReporteResumenProductosDTO;
import com.virtualferia.models.dto.ReporteTopProductoresDTO;
import com.virtualferia.models.dto.ResumenVentaDTO;
import com.virtualferia.models.dto.TotalComprasDTO;

@Service
public class ReporteServiceImpl implements IReporteService {

	
	@Autowired
	IReporteDao reporteDao;

	@Override
	public TotalComprasDTO TotalVentas() {
		
		return reporteDao.TotalCompras();
	}

	@Override
	public List<ReporteCompraDTO> TopTresCompradores() {
		return reporteDao.TopTresCompradores();
	}

	@Override
	public List<ResumenVentaDTO> ResumenAnualVentas() {
		return null;
	}

	@Override
	public ReporteResumenProductoresDTO ResumenRegVenta() {
		return reporteDao.ResumenRegVenta();
	}

	@Override
	public List<ReporteTopProductoresDTO> TopProductores() {
		return reporteDao.TopProductores();
	}

	@Override
	public List<ReporteResumenProductosDTO> ResumenProductos() {
		return reporteDao.ResumenProductos();
	}

}
