package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Contrato;


public interface IContratoService {
	
	public List<Contrato> listar();
	
	public void guardar(Contrato pago);
	 
	public Contrato buscarPorId(Long id);
	
	public void eliminar (Long id);

}
