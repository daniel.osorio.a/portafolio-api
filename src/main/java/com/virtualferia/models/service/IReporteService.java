package com.virtualferia.models.service;

import java.util.List;
import com.virtualferia.models.dto.ReporteCompraDTO;
import com.virtualferia.models.dto.ReporteResumenProductoresDTO;
import com.virtualferia.models.dto.ReporteResumenProductosDTO;
import com.virtualferia.models.dto.ReporteTopProductoresDTO;
import com.virtualferia.models.dto.ResumenVentaDTO;
import com.virtualferia.models.dto.TotalComprasDTO;

public interface IReporteService {
	
	
	TotalComprasDTO TotalVentas();
	List<ReporteCompraDTO> TopTresCompradores();
	List<ResumenVentaDTO> ResumenAnualVentas();
	
	
	ReporteResumenProductoresDTO ResumenRegVenta();
	List<ReporteTopProductoresDTO> TopProductores();
	
	List<ReporteResumenProductosDTO> ResumenProductos();
	

}
