package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Pago;

public interface IPagoService {
		
	public List<Pago> listar();
	
	public void guardar(Pago pago);
	
	public boolean pagar(Pago pago);
	 
	public Pago buscarPorId(Long id);
	
	public Pago buscarPorIdCompra(Long idCompra);
	
	public void eliminar (Long id);
	

}
