package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.ITransporteDao;
import com.virtualferia.models.entity.Transporte;

@Service
public class TransporteServiceImpl implements ITransporteService {

	
	@Autowired
	ITransporteDao transporteDao;
	
	@Override
	public List<Transporte> listar() {
		// TODO Auto-generated method stub
		
		return (List<Transporte>)transporteDao.findAll(); 
	}

	@Override
	public void guardar(Transporte transporte) {
		// TODO Auto-generated method stub
		transporteDao.save(transporte);
		
	}

	@Override
	public Transporte buscarPorId(Long id) {
		// TODO Auto-generated method stub
		
		return transporteDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		transporteDao.deleteById(id);;
		// TODO Auto-generated method stub
		
	}
	
	
	

}
