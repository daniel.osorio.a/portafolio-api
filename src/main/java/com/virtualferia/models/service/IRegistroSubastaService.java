package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.RegistroSubasta;

public interface IRegistroSubastaService {
	public List<RegistroSubasta> listar();
	
	public void guardar(RegistroSubasta registroSubasta);
	 
	public RegistroSubasta buscarPorId(Long id);
	
	public void eliminar (Long id);
}
