package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IRegistroVentaProductorDao;
import com.virtualferia.models.entity.RegistroVentaProductor;

@Service
public class RegistroVentaProductorServiceImpl implements IRegistroVentaProductorService {

	@Autowired
	IRegistroVentaProductorDao registroVentaProductorDao;
	
	@Override
	public List<RegistroVentaProductor> listar() {
		// TODO Auto-generated method stub
		return (List<RegistroVentaProductor>) registroVentaProductorDao.findAll() ;
	}

	@Override
	public void guardar(RegistroVentaProductor registroVentaProductor) {
		// TODO Auto-generated method stub
		registroVentaProductorDao.save(registroVentaProductor);
	}

	@Override
	public RegistroVentaProductor buscar(Long id) {
		// TODO Auto-generated method stub
		return registroVentaProductorDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		registroVentaProductorDao.deleteById(id);
	}

	@Override
	public void Actualizar(RegistroVentaProductor registroVentaProductor) {
		// TODO Auto-generated method stub
		
	}

}
