package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IProductoDao;
import com.virtualferia.models.dao.IProductoSPDao;
import com.virtualferia.models.entity.Producto;

@Service
public class PorductoServiceImpl  implements IProuctoService{
	
	
	@Autowired
	private IProductoDao productoDao;
	
	@Autowired
	private IProductoSPDao productoDaoSp;

	@Override
	public List<Producto> listar() {
		
		return   (List<Producto>) productoDao.findAll();
	}
	
	@Override
	public List<Producto> buscarPorNombre(String nombre) {
		// TODO Auto-generated method stub
		return productoDao.buscarPorNombre(nombre);
	}

	@Override
	public void guardar(Producto producto) {
		// TODO Auto-generated method stub
		productoDao.save(producto);
		
	}
	

	@Override
	public Producto buscarUno(Long id) {
		// TODO Auto-generated method stub
		return productoDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		productoDao.deleteById(id);
	}

	@Override
	public Producto buscarUnoSP(Long id) {
		// TODO Auto-generated method stub
		return productoDaoSp.findOne(id);
	}

	@Override
	public void eliminarSP(Long id) {
		// TODO Auto-generated method stub
		productoDaoSp.delete(id);
		
	} 
	
	@Override
	public void guardarSP(Producto producto) {
		// TODO Auto-generated method stub
		productoDaoSp.save(producto);
		
	}

	@Override
	public void ActualizarSP(Producto producto) {
		// TODO Auto-generated method stub
		productoDaoSp.update(producto);
		
	}

	@Override
	public List<Producto> listarSP() {
		// TODO Auto-generated method stub
		return productoDaoSp.findAll();
		
	}

}
