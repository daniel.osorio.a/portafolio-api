package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IProductorDao;
import com.virtualferia.models.entity.Productor;

@Service
public class ProductorServiceImpl implements IProductorService{
	
	@Autowired
	IProductorDao productorDao;

	@Override
	public List<Productor> listar() {
		// TODO Auto-generated method stub
		
		return (List<Productor>)productorDao.findAll();
	}

	@Override
	public void guardar(Productor productor) {
		// TODO Auto-generated method stub
		productorDao.save(productor);
	}

	@Override
	public Productor buscar(Long id) {
		// TODO Auto-generated method stub
		return productorDao.findById(id).orElse(null);
	}
	
	@Override
	public Productor buscarPorUsuarioId(Long id) {
		// TODO Auto-generated method stub
		return productorDao.buscarPorUsuarioId(id);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		productorDao.deleteById(id);
	}

	@Override
	public void Actualizar(Productor productor) {
		// TODO Auto-generated method stub
		
	}

	

}
