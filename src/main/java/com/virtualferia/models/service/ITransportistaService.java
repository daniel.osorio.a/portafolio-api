package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Transportista;


public interface ITransportistaService {
	
	public List<Transportista> listar();
	
	
	public void guardar(Transportista transportista);
	 
	public Transportista buscarPorId(Long id);
	
	
	public Transportista buscarPorUsuarioId (Long id);
	
	public void eliminar (Long id);

}
