package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.dto.SubastaDTO;
import com.virtualferia.models.entity.RegistroSubasta;
import com.virtualferia.models.entity.Subasta;

public interface ISubastaService {
	
	public List<Subasta> listar();
	
	public void guardar(Subasta subasta);
	 
	public Subasta buscarPorId(Long id);
	
	public void eliminar (Long id);
	
	public List<Subasta> listarSoloSubastas();

	public RegistroSubasta MejorTransporte(Long IdSubasta); 
	
	public RegistroSubasta MejorTransporte(List<RegistroSubasta> Registros); 
	
	public List<Subasta> SubastasGanadasPorUsuarioId(Long IdUsuario);
	
	public boolean CompletarPedido(Subasta idSubasta);
}
