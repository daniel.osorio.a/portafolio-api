package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IRegistroSubastaDao;
import com.virtualferia.models.entity.RegistroSubasta;

@Service
public class RegistroSubastaServiceImpl implements IRegistroSubastaService {
	
	@Autowired
	IRegistroSubastaDao registroSubastaDao;

	@Override
	public List<RegistroSubasta> listar() {
		// TODO Auto-generated method stub
		return (List<RegistroSubasta>)registroSubastaDao.findAll();
	}

	@Override
	public void guardar(RegistroSubasta registroSubasta) {
		// TODO Auto-generated method stub
		registroSubastaDao.save(registroSubasta);
	}

	@Override
	public RegistroSubasta buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return registroSubastaDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		registroSubastaDao.deleteById(id);
	}

	 

	 

}
