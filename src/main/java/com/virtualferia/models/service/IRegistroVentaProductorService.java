package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.RegistroVentaProductor;;

public interface IRegistroVentaProductorService {
	
	public List<RegistroVentaProductor> listar();
	
	public void guardar(RegistroVentaProductor registroVentaProductor);
	 
	public RegistroVentaProductor buscar(Long id);
	
	public void eliminar (Long id);
	
	public void Actualizar(RegistroVentaProductor registroVentaProductor); 
}


