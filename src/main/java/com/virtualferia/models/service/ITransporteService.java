package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Transporte;

public interface ITransporteService {
	
	public List<Transporte> listar();
	
	
	public void guardar(Transporte transporte);
	 
	public Transporte buscarPorId(Long id);
	
	public void eliminar (Long id);

}
