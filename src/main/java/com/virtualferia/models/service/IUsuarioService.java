package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Usuario;


public interface IUsuarioService {
	
	List<Usuario> listar();
	
	List<Usuario> listarSP();
	
	public void guardar(Usuario usaurio);
	 
	public void guardarSP(Usuario usuario); 
	
	public Usuario buscarUno(Long id);
	
	public Usuario buscarUnoSP(Long id);
	
	public void eliminar (Long id);
	
	public void eliminarSP (Long id);
	
	public void ActualizarSP(Usuario producto); 
	
	public Usuario login(String usuario , String contrasena);

}
