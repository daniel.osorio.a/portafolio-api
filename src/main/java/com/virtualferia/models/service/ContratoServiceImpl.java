package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IContratoDao;
import com.virtualferia.models.entity.Contrato;

@Service
public class ContratoServiceImpl  implements IContratoService{
	
	@Autowired
	IContratoDao contratoDao;
	
	@Override
	public List<Contrato> listar() {
		// TODO Auto-generated method stub
		return (List<Contrato>)contratoDao.findAll();
	}

	@Override
	public void guardar(Contrato pago) {
		// TODO Auto-generated method stub
		contratoDao.save(pago);
	}

	@Override
	public Contrato buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return contratoDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		contratoDao.deleteById(id);
		
	}

}
