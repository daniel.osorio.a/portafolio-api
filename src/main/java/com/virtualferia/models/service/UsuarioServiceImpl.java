package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IUsuarioDao;
import com.virtualferia.models.entity.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	IUsuarioDao usuarioDao;

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return  (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	public List<Usuario> listarSP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardar(Usuario usuario) {
		// TODO Auto-generated method stub
		usuarioDao.save(usuario);
	}

	@Override
	public void guardarSP(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario buscarUno(Long id) {
		// TODO Auto-generated method stub
		
		return usuarioDao.findById(id).orElse(null);
	}

	@Override
	public Usuario buscarUnoSP(Long id) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		usuarioDao.deleteById(id);
	}

	@Override
	public void eliminarSP(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ActualizarSP(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario login(String usuario , String contrasena) {
		// TODO Auto-generated method stub
		
		
		return usuarioDao.buscarPortUserPass(usuario,contrasena);
	}

}
