package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.Comprador;

public interface ICompradorService {
	
	public List<Comprador> listar();
	
	//public Page<Comprador> findAll(Pageable pageable);
	
	public void guardar(Comprador cliente);
	 
	public Comprador buscarPorId(Long id);
	
	public void eliminar (Long id);
	
	public Comprador buscarPorUsuarioId(Long idUsuario);

}
