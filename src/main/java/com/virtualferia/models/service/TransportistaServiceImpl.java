package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.ITransportistaDao;
import com.virtualferia.models.entity.Transportista;

@Service
public class TransportistaServiceImpl implements ITransportistaService{

	@Autowired
	ITransportistaDao  transportistaDao;
	
	@Override
	public List<Transportista> listar() {
		// TODO Auto-generated method stub
		return (List<Transportista>) transportistaDao.findAll();
	}

	@Override
	public void guardar(Transportista transportista) {
		// TODO Auto-generated method stub
		transportistaDao.save(transportista);
		
	}

	@Override
	public Transportista buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return transportistaDao.findById(id).orElse(null);
	}
	
	@Override
	public Transportista buscarPorUsuarioId(Long id) {
		// TODO Auto-generated method stub
		return transportistaDao.buscarPorUsuarioId(id);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		transportistaDao.deleteById(id);
	}

}
