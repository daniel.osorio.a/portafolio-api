package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.ISubastaDao;
import com.virtualferia.models.dao.ITransporteDao;
import com.virtualferia.models.dao.Subasta.ISubastaSPDao;
import com.virtualferia.models.dto.SubastaDTO;
import com.virtualferia.models.entity.RegistroSubasta;
import com.virtualferia.models.entity.Subasta;
import com.virtualferia.models.entity.Transporte;


@Service
public class SubastaServiceImpl implements ISubastaService{
	
	@Autowired
	ISubastaDao subastaDao;
	
	@Autowired
	ISubastaSPDao subastaSPDao;
	
	@Autowired
	ITransporteDao transporteaDao;
	
	@Autowired
	IVentaService compraService;

	@Override
	public List<Subasta> listar() {
		// TODO Auto-generated method stub
		return (List<Subasta>) subastaDao.findAll();
	}

	@Override
	public void guardar(Subasta subasta) {
		// TODO Auto-generated method stub
		subastaDao.save(subasta);
	}

	@Override
	public Subasta buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return subastaDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		subastaDao.deleteById(id);
	}

	@Override
	public List<Subasta> listarSoloSubastas() {
		// TODO Auto-generated method stub
		return (List<Subasta>) subastaDao.listarSoloSubastas();
	}

	@Override
	public RegistroSubasta MejorTransporte(List<RegistroSubasta> Registros) {
		
		RegistroSubasta resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroSubasta registroSubasta : Registros) {
			
			Transporte transporte = transporteaDao.findById(registroSubasta.getId_transporte()).orElse(null);
			
			if (transporte != null) {
				if (transporte.getPrecio() <= menorPrecio) {
					menorPrecio = transporte.getPrecio();
					resultadoMenorPrecio = registroSubasta;
				}
			}
			
		}
		
		
		return resultadoMenorPrecio;
	}

	
	@Override
	public List<Subasta> SubastasGanadasPorUsuarioId(Long IdUsuario) {
		return subastaSPDao.SubastasGanadasPorUsuarioId(IdUsuario);
	}
	
	@Override
	public RegistroSubasta MejorTransporte(Long IdSubasta) {
		Subasta subasta = subastaDao.findById(IdSubasta).orElse(null);
		
		if (subasta == null) {	
			return null;
		}
		
		RegistroSubasta resultadoMenorPrecio = null;
		Double menorPrecio = 0.0;
		
		for (RegistroSubasta registroSubasta : subasta.getRegistros()) {
			
			Transporte transporte = transporteaDao.findById(registroSubasta.getId_transporte()).orElse(null);
			
			if (transporte != null) {
				if (transporte.getPrecio() <= menorPrecio) {
					menorPrecio = transporte.getPrecio();
					resultadoMenorPrecio = registroSubasta;
				}
			}
			
		}
		
		
		return resultadoMenorPrecio;
	}

	@Override
	public boolean CompletarPedido(Subasta subasta) {
		boolean resultado = false;
		
		resultado = compraService.CompletarPedido(subasta.getIdSubasta());
				 
		if (resultado) {	
			return false;
		}
				
		return resultado;
	}


	
	 


}
