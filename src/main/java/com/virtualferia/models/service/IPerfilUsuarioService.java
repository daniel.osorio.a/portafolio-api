package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.PerfilUsuario;

public interface IPerfilUsuarioService {
	
	public List<PerfilUsuario> listar();
	
	public void guardar(PerfilUsuario perfilUsuario);
	 
	public PerfilUsuario buscarPorId(Long id);
	
	public void eliminar (Long id);


}
