package com.virtualferia.models.service;

 

import java.util.List;

import com.virtualferia.models.entity.Producto;


public interface IProuctoService   {
	
	 
	List<Producto> buscarPorNombre(String nombre);
	
	List<Producto> listar();
	
	List<Producto> listarSP();
	
	public void guardar(Producto producto);
	 
	public void guardarSP(Producto producto); 
	
	public Producto buscarUno(Long id);
	
	public Producto buscarUnoSP(Long id);
	
	public void eliminar (Long id);
	
	public void eliminarSP (Long id);
	
	public void ActualizarSP(Producto producto); 
	
	
	
}
