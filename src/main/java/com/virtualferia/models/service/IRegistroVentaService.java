package com.virtualferia.models.service;

import java.util.List;

import com.virtualferia.models.entity.RegistroVenta;
import com.virtualferia.models.entity.RegistroVentaProductor;

public interface IRegistroVentaService {
	
	
	public List<RegistroVenta> listar();
	
	public void guardar(RegistroVenta registroVenta);
	 
	public RegistroVenta buscar(Long id);
	
	public void eliminar (Long id);
	
	public void ActualizarSP(RegistroVenta registroVenta); 
	
	public RegistroVentaProductor MejorProductor(Long IdRegistroVenta);
}
