package com.virtualferia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtualferia.models.dao.IPerfilUsuarioDao;
import com.virtualferia.models.entity.PerfilUsuario;

@Service
public class PerfilUsuarioService implements IPerfilUsuarioService {

	
	@Autowired
	IPerfilUsuarioDao perfilUsuarioDao;

	
	
	@Override
	public List<PerfilUsuario> listar() {
		// TODO Auto-generated method stub
		return (List<PerfilUsuario>) perfilUsuarioDao.findAll();
	}

	@Override
	public void guardar(PerfilUsuario perfilUsuario) {
		// TODO Auto-generated method stub
		perfilUsuarioDao.save(perfilUsuario);
		
	}

	@Override
	public PerfilUsuario buscarPorId(Long id) {
		// TODO Auto-generated method stub
		return perfilUsuarioDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		perfilUsuarioDao.deleteById(id);
	}

}
