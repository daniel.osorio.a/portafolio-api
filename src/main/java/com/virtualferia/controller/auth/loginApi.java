package com.virtualferia.controller.auth;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Usuario;
import com.virtualferia.models.service.IUsuarioService;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/login")
public class loginApi {
	
	@Autowired
	IUsuarioService usuarioService;
	
	
	
	@PostMapping("ingresar")
	  public ResponseEntity<Usuario>  Ingresar(@Valid @RequestBody Map<String, String> login) {
		
		Usuario usuario = usuarioService.login(login.get("usuario"), login.get("contrasena"));
		
		
		if (usuario != null) {
			usuario.setContrasena("");
			 return ResponseEntity.ok().body(usuario);
			 
		}
		return ResponseEntity.notFound().build();
	  }

}
