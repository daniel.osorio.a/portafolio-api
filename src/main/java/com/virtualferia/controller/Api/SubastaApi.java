package com.virtualferia.controller.Api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.dto.SubastaDTO;
import com.virtualferia.models.entity.Subasta;
import com.virtualferia.models.service.ISubastaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/subasta")
public class SubastaApi {
	
	@Autowired
	ISubastaService subastaService;
	
	@GetMapping("listar")
    public List<Subasta> ListarProductos(){
        return subastaService.listar();
    }
	
	@GetMapping("listar2")
    public List<Subasta> ListarProductos2(){
        return subastaService.listarSoloSubastas();
    }
	
	@GetMapping("subastasGanadas/{id}")
    public List<Subasta> SubastasGanadas(@PathVariable(value = "id") Long IdUsuario){
        return subastaService.SubastasGanadasPorUsuarioId(IdUsuario);
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Subasta> ObtenerPorId(@PathVariable(value = "id") Long subastaId)
	  {
		Subasta subasta = subastaService.buscarPorId(subastaId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (subasta != null) {
			 return ResponseEntity.ok().body(subasta);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Subasta subasta) {
		subastaService.guardar(subasta);
	  }
	
	@PostMapping("completarPedido")
	  public void CompletarPedido(@Valid @RequestBody Subasta subasta) {
		subastaService.CompletarPedido(subasta);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Subasta> Actualizar(
	      @PathVariable(value = "id") Long subastaId, @Valid @RequestBody Subasta subastaIn)
	       {
		 
		Subasta subasta = subastaService.buscarPorId(subastaId);
		
		
		subasta.setFechaInicio(subastaIn.getFechaInicio());
		subasta.setFechaFin(subastaIn.getFechaFin());
		subasta.setRegistros(subastaIn.getRegistros());
		
		subastaService.guardar(subasta);
	    
	    return ResponseEntity.ok(subasta);
	  }

}
