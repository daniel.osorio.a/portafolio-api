package com.virtualferia.controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.dto.ReporteCompraDTO;
import com.virtualferia.models.dto.ReporteResumenProductoresDTO;
import com.virtualferia.models.dto.ReporteResumenProductosDTO;
import com.virtualferia.models.dto.ReporteTopProductoresDTO;
import com.virtualferia.models.dto.TotalComprasDTO;
import com.virtualferia.models.service.IReporteService;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/reporte")
public class ReporteApi {
	
	
	@Autowired
	IReporteService reporteService;

	
	@GetMapping("/resumenCompras")
	  public ResponseEntity<TotalComprasDTO> TotalVentas()
	  {
		TotalComprasDTO totalVentas =  reporteService.TotalVentas();
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    return ResponseEntity.ok().body(totalVentas);
	  }
	
	@GetMapping("/topCompras")
	  public ResponseEntity<List<ReporteCompraDTO>> TopCompras()
	  {
		List<ReporteCompraDTO>  reporteCompraDTO =  reporteService.TopTresCompradores();
		
	    return ResponseEntity.ok().body(reporteCompraDTO);
	  }
	
	@GetMapping("/resumenProductores")
	  public ResponseEntity<ReporteResumenProductoresDTO> ResumeRegistroVenta()
	  {
		ReporteResumenProductoresDTO reporteResumenProductores =  reporteService.ResumenRegVenta();
		
	    return ResponseEntity.ok().body(reporteResumenProductores);
	  }
	
	@GetMapping("/topProductores")
	  public ResponseEntity<List<ReporteTopProductoresDTO>> TopProductores()
	     {
		List<ReporteTopProductoresDTO>   reporteTopProductores =  reporteService.TopProductores();
		
	    return ResponseEntity.ok().body(reporteTopProductores);
	  }
	
	@GetMapping("/resumenProductos")
	  public ResponseEntity<List<ReporteResumenProductosDTO>> ResumenProductos()
	  {
		List<ReporteResumenProductosDTO>  resumenProductos =  reporteService.ResumenProductos();
		
	    return ResponseEntity.ok().body(resumenProductos);
	  }
	
}
