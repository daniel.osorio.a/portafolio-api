package com.virtualferia.controller.Api;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.virtualferia.models.entity.Compra;
import com.virtualferia.models.service.IVentaService;

@RestController
@CrossOrigin(origins = "*")
@SessionAttributes("venta") 
@RequestMapping("/api/venta")
public class CompraApi {

	@Autowired
	IVentaService ventaService;
	
	@GetMapping("listar")
    public ResponseEntity<List<Compra>> ListarProductos(){
		List<Compra> resultado =  ventaService.listar();
        
        if (resultado != null) {
			 return ResponseEntity.ok().body(resultado);
		}
		
	    return ResponseEntity.notFound().build();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Compra> ObtenerPorId(@PathVariable(value = "id") Long ventaId)
	  {
		Compra venta = ventaService.buscarPorId(ventaId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (venta != null) {
			 return ResponseEntity.ok().body(venta);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Compra venta) {
		ventaService.guardar(venta);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Compra> Actualizar(
	      @PathVariable(value = "id") Long ventaId, @Valid @RequestBody Compra ventaIn)
	   {
		Compra venta = ventaService.buscarPorId(ventaId);
		venta.setEstadoVenta(ventaIn.getEstadoVenta());
		venta.setCostoTotal(ventaIn.getCostoTotal());
		
		venta.setRegistrosCompra(ventaIn.getRegistrosCompra());
		venta.setSubasta(ventaIn.getSubasta());
		
		ventaService.guardar(venta);
		 
	    
	    return ResponseEntity.ok(venta);
	  }
	
	 @DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> eliminaProducto(@PathVariable(value = "id") Long ventaId) throws Exception {
		 Compra venta =  ventaService.buscarPorId(ventaId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    if (venta != null) {
	    	ventaService.eliminar(ventaId);
		}
	   
	    
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("eliminado", Boolean.TRUE);
	    return response;
	  }

}
