package com.virtualferia.controller.Api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Transporte;
import com.virtualferia.models.service.ITransporteService;


@RestController
@RequestMapping("/api/transporte")
public class TransporteApi {
	

	@Autowired
	ITransporteService transporteService;
	
	@GetMapping("listar")
    public List<Transporte> ListarProductos(){
        return transporteService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Transporte> ObtenerPorId(@PathVariable(value = "id") Long transporteId)
	  {
		Transporte transporte = transporteService.buscarPorId(transporteId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (transporte != null) {
			 return ResponseEntity.ok().body(transporte);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Transporte transporte) {
		transporteService.guardar(transporte);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Transporte> Actualizar(
	      @PathVariable(value = "id") Long usuarioId, @Valid @RequestBody Transporte transporteIn)
	       {
		 
		Transporte transporte = transporteService.buscarPorId(usuarioId);
		
		transporte.setPrecio(transporteIn.getPrecio());
		transporte.setRegrigeracion(transporteIn.getRegrigeracion()); 
		transporte.setTamano(transporteIn.getTamano()); 
		transporte.setCapacidad(transporteIn.getCapacidad()); 
		
		 
		 
		transporteService.guardar(transporte);
		 
	    
	    return ResponseEntity.ok(transporte);
	  }

}
