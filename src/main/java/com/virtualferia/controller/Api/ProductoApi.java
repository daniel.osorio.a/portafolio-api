package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Producto;
import com.virtualferia.models.service.IProuctoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/producto")
public class ProductoApi {
	
	@Autowired
	IProuctoService prouctoService;
	
	@GetMapping("listar")
    public List<Producto> listarProductos(){
        return prouctoService.listar();
    }
	
	@GetMapping("listarSP")
    public List<Producto> listarProductosSP(){
        return prouctoService.listarSP();
    }
	
//	Ejemplo Post Guardar Postman 
//	{ 
//		"idProducto" : 2 ,
//		"nombre" : "prod one",
//		"volumen" : 0.0 ,
//		"peso" : 0.0 
//		
//	}
	
	@GetMapping("/buscar/{id}")
	  public ResponseEntity<Producto> obtenerProducto(@PathVariable(value = "id") Long productoId)
	  {
		Producto producto = prouctoService.buscarUno(productoId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    return ResponseEntity.ok().body(producto);
	  }
	
	  @GetMapping("/buscarSP/{id}")
	  public ResponseEntity<Producto> obtenerProductoSP(@PathVariable(value = "id") Long productoId)
	     {
		Producto producto =  prouctoService.buscarUnoSP(productoId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    return ResponseEntity.ok().body(producto);
	  }
	
	  @PostMapping("guardar")
	  public void creaProducto(@Valid @RequestBody Producto producto) {
		prouctoService.guardar(producto);
	  }
	
	  @PostMapping("guardarSP")
	  public void creaProductoSP(@Valid @RequestBody Producto producto) {
		prouctoService.guardarSP(producto);
	  }
	
	 @PutMapping("actualizar/{id}")
	  public ResponseEntity<Producto> actualizaProducto(
	      @PathVariable(value = "id") Long productoId, @Valid @RequestBody Producto productoInput)
	       {
		 
		 Producto producto = prouctoService.buscarUno(productoId);
		 producto.setNombre(productoInput.getNombre()); 
		 producto.setPeso(productoInput.getPeso());
		 producto.setVolumen(productoInput.getVolumen());
		 
		 prouctoService.guardar(producto);
		 
	    
	    return ResponseEntity.ok(producto);
	  }
	 
	 @PutMapping("actualizarSP/{id}")
	  public ResponseEntity<Producto> actualizaProductoSP(
	      @PathVariable(value = "id") Long productoId, @Valid @RequestBody Producto productoInput)
	       {
		 
		 Producto producto = prouctoService.buscarUnoSP(productoId);
		 producto.setNombre(productoInput.getNombre()); 
		 producto.setPeso(productoInput.getPeso());
		 producto.setVolumen(productoInput.getVolumen());
		 
		 prouctoService.ActualizarSP(producto);
		 
	    
	    return ResponseEntity.ok(producto);
	  }
	 
	 
	 @DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> eliminaProducto(@PathVariable(value = "id") Long productoId) throws Exception {
	    Producto prdocuto =  prouctoService.buscarUno(productoId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    if (prdocuto != null) {
	    	 prouctoService.eliminar(productoId);
		}
	   
	    
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("eliminado", Boolean.TRUE);
	    return response;
	  }
	
	 
	 @DeleteMapping("eliminarSP/{id}")
	  public Map<String, Boolean> eliminaProductoSP(@PathVariable(value = "id") Long productoId) throws Exception {
	    Producto prdocuto =  prouctoService.buscarUnoSP(productoId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	   
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (prdocuto != null) {
	    	 prouctoService.eliminarSP(productoId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			
			response.put("Producto no encontrado ", Boolean.TRUE);
		}
	    
	    
	    
	    return response;
	  }
	 

}
