package com.virtualferia.controller.Api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.RegistroSubasta;
import com.virtualferia.models.service.IRegistroSubastaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/registroSubasta")
public class RegistroSubastaApi {
	
	@Autowired
	IRegistroSubastaService registroSubastaService;
	
	@GetMapping("listar")
    public List<RegistroSubasta> ListarProductos(){
        return registroSubastaService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<RegistroSubasta> ObtenerPorId(@PathVariable(value = "id") Long registroSubastaId)
	  {
		RegistroSubasta registroSubasta = registroSubastaService.buscarPorId(registroSubastaId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (registroSubasta != null) {
			 return ResponseEntity.ok().body(registroSubasta);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody RegistroSubasta registroSubasta) {
		registroSubastaService.guardar(registroSubasta);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<RegistroSubasta> Actualizar(
	      @PathVariable(value = "id") Long registroSubastaId, @Valid @RequestBody RegistroSubasta registroSubastaIn)
	       {
		 
		RegistroSubasta registroSubasta = registroSubastaService.buscarPorId(registroSubastaId);
		
		
		registroSubasta.setFechaRegistro(registroSubastaIn.getFechaRegistro());
		
		registroSubastaService.guardar(registroSubasta);
	    
	    return ResponseEntity.ok(registroSubasta);
	  }

}
