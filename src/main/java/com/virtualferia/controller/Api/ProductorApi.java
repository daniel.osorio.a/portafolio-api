package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Productor;
import com.virtualferia.models.service.IProductorService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/productor")
public class ProductorApi {

	@Autowired
	IProductorService productorService;

	@GetMapping("listar")
	public List<Productor> ListarProductos() {
		return productorService.listar();
	}

	@GetMapping("buscar/{id}")
	public ResponseEntity<Productor> ObtenerPorId(@PathVariable(value = "id") Long productorId) {
		Productor productor = productorService.buscar(productorId);
		// .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " +
		// userId));

		if (productor != null) {
			return ResponseEntity.ok().body(productor);
		}

		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("buscarPorUsuario/{id}")
	public ResponseEntity<Productor> ObtenerPorUsuarioId(@PathVariable(value = "id") Long productorId) {
		Productor productor = productorService.buscarPorUsuarioId(productorId);
		// .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " +
		// userId));

		if (productor != null) {
			return ResponseEntity.ok().body(productor);
		}

		return ResponseEntity.notFound().build();
	}
	
	

	@PostMapping("guardar")
	public void Crear(@Valid @RequestBody Productor productor) {
		productorService.guardar(productor);
	}

	@PutMapping("actualizar/{id}")
	public ResponseEntity<Productor> Actualizar(@PathVariable(value = "id") Long productorId,
			@Valid @RequestBody Productor productorIn) {

		Productor productor = productorService.buscar(productorId);

		productor.setVentas(productorIn.getVentas());
		
		productorService.guardar(productor);

		return ResponseEntity.ok(productor);
	}

	@DeleteMapping("eliminar/{id}")
	public Map<String, Boolean> elimina(@PathVariable(value = "id") Long productorId) throws Exception {
		Productor productor = productorService.buscar(productorId);
		// .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " +
		// userId));

		Map<String, Boolean> response = new HashMap<>();

		if (productor != null) {
			productorService.eliminar(productorId);
			response.put("eliminado", Boolean.TRUE);
		} else {
			response.put("No eliminado", Boolean.FALSE);
		}

		return response;
	}

}
