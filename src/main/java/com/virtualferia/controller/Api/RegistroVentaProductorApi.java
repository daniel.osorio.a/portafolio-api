package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.RegistroVentaProductor;
import com.virtualferia.models.service.IRegistroVentaProductorService;
import com.virtualferia.models.service.IVentaService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/registroVentaProductor")
public class RegistroVentaProductorApi {

	@Autowired
	IRegistroVentaProductorService registroVentaProductorService;
	
//	@Autowired
//	IRegistroVentaService registroVentaService;
	
	@Autowired
	IVentaService VentaService;
	
	@GetMapping("listar")
    public List<RegistroVentaProductor> ListarProductos(){
        return registroVentaProductorService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<RegistroVentaProductor> ObtenerPorId(@PathVariable(value = "id") Long registroVentaProductorID)
	  {
		RegistroVentaProductor registroVentaProductor = registroVentaProductorService.buscar(registroVentaProductorID);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (registroVentaProductor != null) {
			 return ResponseEntity.ok().body(registroVentaProductor);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody RegistroVentaProductor registroVentaProductor) {
		registroVentaProductorService.guardar(registroVentaProductor);
		
		
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<RegistroVentaProductor> Actualizar(
	      @PathVariable(value = "id") Long registroVentaProductorId, @Valid @RequestBody RegistroVentaProductor registroVentaProductorIn)
	       {
		 
		RegistroVentaProductor registroVentaProductor = registroVentaProductorService.buscar(registroVentaProductorId);
		
		
		registroVentaProductor.setCantidad(registroVentaProductorIn.getCantidad());
		registroVentaProductor.setPrecio(registroVentaProductorIn.getPrecio());
		registroVentaProductor.setIdRegistroVentaProductor(registroVentaProductorIn.getIdRegistroVentaProductor());
		
		registroVentaProductorService.guardar(registroVentaProductor);
	    	
	    return ResponseEntity.ok(registroVentaProductor);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long registroVentaProductorId) throws Exception {
		RegistroVentaProductor registroVentaProductor = registroVentaProductorService.buscar(registroVentaProductorId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (registroVentaProductor != null) {
	    	registroVentaProductorService.eliminar(registroVentaProductorId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	   
	    return response;
	  }
	
	
	
}
