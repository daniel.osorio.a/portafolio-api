package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Contrato;
import com.virtualferia.models.service.IContratoService;


@RestController
@RequestMapping("/api/contrato")
public class ContratoApi {
	
	@Autowired
	IContratoService ContratoService;
	
	@GetMapping("listar")
    public List<Contrato> ListarProductos(){
        return ContratoService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Contrato> ObtenerPorId(@PathVariable(value = "id") Long contratoId)
	  {
		Contrato contrato = ContratoService.buscarPorId(contratoId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (contrato != null) {
			 return ResponseEntity.ok().body(contrato);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Contrato contrato) {
		ContratoService.guardar(contrato);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Contrato> Actualizar(
	      @PathVariable(value = "id") Long contratoId, @Valid @RequestBody Contrato contratoIn)
	       {
		 
		Contrato contrato = ContratoService.buscarPorId(contratoId);
		
		contrato.setDescripcion(contratoIn.getDescripcion()); 
		contrato.setFechaInicio(contratoIn.getFechaInicio());
		contrato.setFechaTermino(contratoIn.getFechaTermino());
		contrato.setNombre(contratoIn.getNombre());
		
		ContratoService.guardar(contrato);
	    	
	    return ResponseEntity.ok(contrato);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long contratoId) throws Exception {
	    Contrato contrato = ContratoService.buscarPorId(contratoId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (contrato != null) {
	    	 ContratoService.eliminar(contratoId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	   
	    
	    
	    
	    return response;
	  }

}
