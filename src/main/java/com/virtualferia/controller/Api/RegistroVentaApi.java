package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.virtualferia.models.entity.RegistroVenta;
import com.virtualferia.models.service.IRegistroVentaService;


@RestController
@CrossOrigin(origins = "*")
@SessionAttributes("registroVenta") 
@RequestMapping("/api/registroVenta")
public class RegistroVentaApi {

	@Autowired
	IRegistroVentaService registroVentaService;
	
	@GetMapping("listar")
    public List<RegistroVenta> ListarProductos(){
        return registroVentaService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<RegistroVenta> ObtenerPorId(@PathVariable(value = "id") Long registroVentaId)
	  {
		RegistroVenta registroVenta = registroVentaService.buscar(registroVentaId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (registroVenta != null) {
			 return ResponseEntity.ok().body(registroVenta);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody RegistroVenta registroVenta) {
		registroVentaService.guardar(registroVenta);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<RegistroVenta> Actualizar(
	      @PathVariable(value = "id") Long ventaId, @Valid @RequestBody RegistroVenta registroVentaIn)
	       {
		 
		RegistroVenta registroVenta = registroVentaService.buscar(ventaId);
		
		registroVenta.setCantidad(registroVentaIn.getCantidad());
		registroVenta.setProducto(registroVentaIn.getProducto());
		
		registroVentaService.guardar(registroVenta);
	    	
	    return ResponseEntity.ok(registroVenta);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long registroVentaId) throws Exception {
	    RegistroVenta registroVenta = registroVentaService.buscar(registroVentaId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (registroVenta != null) {
	    	 registroVentaService.eliminar(registroVentaId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	    
	    return response;
	  }
	
	
}
