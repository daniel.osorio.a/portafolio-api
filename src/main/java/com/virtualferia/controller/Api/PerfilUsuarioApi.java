package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.PerfilUsuario;
import com.virtualferia.models.service.IPerfilUsuarioService;

@RestController
@RequestMapping("/api/perfilUsuario")
public class PerfilUsuarioApi {
	
	@Autowired
	IPerfilUsuarioService perfilUsuarioService;
	
	@GetMapping("listar")
    public List<PerfilUsuario> ListarProductos(){
        return perfilUsuarioService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<PerfilUsuario> ObtenerPorId(@PathVariable(value = "id") Long perfilUsuarioId)
	  {
		PerfilUsuario perfilUsuario = perfilUsuarioService.buscarPorId(perfilUsuarioId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (perfilUsuario != null) {
			 return ResponseEntity.ok().body(perfilUsuario);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody PerfilUsuario perfilUsuario) {
		perfilUsuarioService.guardar(perfilUsuario);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<PerfilUsuario> Actualizar(
	      @PathVariable(value = "id") Long perfilUsuarioId, @Valid @RequestBody PerfilUsuario perfilUsuarioIn)
	       {
		 
		PerfilUsuario perfilUsuario = perfilUsuarioService.buscarPorId(perfilUsuarioId);
		
		perfilUsuario.setNombre(perfilUsuarioIn.getNombre()); 
		perfilUsuario.setDescripcion(perfilUsuario.getDescripcion()); 
		
		
		perfilUsuarioService.guardar(perfilUsuario);
	    	
	    return ResponseEntity.ok(perfilUsuario);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long perfilUsuarioId) throws Exception {
	    PerfilUsuario perfilUsuario = perfilUsuarioService.buscarPorId(perfilUsuarioId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (perfilUsuario != null) {
	    	 perfilUsuarioService.eliminar(perfilUsuarioId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	   
	    
	    
	    
	    return response;
	  }


}
