package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Usuario;
import com.virtualferia.models.service.IUsuarioService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/usuario")
public class UsuarioApi {
	
	
    @Autowired
    IUsuarioService usuarioService;
	
    @GetMapping("listar")
    public List<Usuario> ListarProductos(){
        return usuarioService.listar();
    }
	
    @GetMapping("buscar/{id}")
    public ResponseEntity<Usuario> ObtenerPorId(@PathVariable(value = "id") Long usuarioId)
    {
	Usuario usuario = usuarioService.buscarUno(usuarioId);
	//.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
        if (usuario != null) {
            return ResponseEntity.ok().body(usuario);
	}
        return ResponseEntity.notFound().build();
    }
	
    @PostMapping("guardar")
    public void Crear(@Valid @RequestBody Usuario usuario) {
        usuarioService.guardar(usuario);
    }
	

    @PatchMapping("actualizar")
    public ResponseEntity<Usuario> Actualizar(@Valid @RequestBody Usuario usuarioIn){
        Usuario usuario = new Usuario();
	try {
            usuario = usuarioService.buscarUno(usuarioIn.getIdUsuario());
            if (usuario != null) {
		usuario.setEmail(usuarioIn.getEmail()); 
		usuario.setNombreUsuario(usuarioIn.getNombreUsuario());
		usuario.setTelefono(usuarioIn.getTelefono());
		usuario.setContrasena(usuarioIn.getContrasena());
		usuario.setContrato(usuarioIn.getContrato());
		usuario.setPagos(usuarioIn.getPagos());
		usuario.setPerfilUsuario(usuarioIn.getPerfilUsuario());
		usuarioService.guardar(usuario);
            }
        } catch (Exception e) {
            // TODO: handle exception
	}
	    
	return ResponseEntity.ok(usuario);
    }
	
	  
    @DeleteMapping("eliminar/{id}")
    public Map<String, Boolean> elimina(@PathVariable(value = "id") Long usuarioId) throws Exception {
        Usuario usuario = usuarioService.buscarUno(usuarioId);
	// .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	Map<String, Boolean> response = new HashMap<>();
	    
	if (usuario != null) {
            usuarioService.eliminar(usuarioId);
            response.put("eliminado", Boolean.TRUE);
        }else {
            response.put("No eliminado", Boolean.FALSE);
	}
        
	return response;
    }

}
