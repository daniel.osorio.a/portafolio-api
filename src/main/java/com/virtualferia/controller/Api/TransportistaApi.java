package com.virtualferia.controller.Api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Transportista;
import com.virtualferia.models.service.ITransportistaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/transportista")
public class TransportistaApi {
	
	@Autowired
	ITransportistaService transportistaService;
	
	@GetMapping("listar")
    public List<Transportista> ListarProductos(){
        return transportistaService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Transportista> ObtenerPorId(@PathVariable(value = "id") Long transportistaId)
	  {
		Transportista usuario = transportistaService.buscarPorId(transportistaId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (usuario != null) {
			 return ResponseEntity.ok().body(usuario);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	
	@GetMapping("buscarPorUsuario/{id}")
	public ResponseEntity<Transportista> ObtenerPorUsuarioId(@PathVariable(value = "id") Long usuarioId) {
		Transportista transportista = transportistaService.buscarPorUsuarioId(usuarioId);
		// .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " +
		// userId));

		if (transportista != null) {
			return ResponseEntity.ok().body(transportista);
		}

		return ResponseEntity.notFound().build();
	}
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Transportista transportista) {
		transportistaService.guardar(transportista);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Transportista> Actualizar(
	      @PathVariable(value = "id") Long usuarioId, @Valid @RequestBody Transportista transportistaIn)
	       {
		 
		Transportista transportista = transportistaService.buscarPorId(usuarioId);
		
		transportista.setTransporte(transportistaIn.getTransporte());
		
		 
		transportistaService.guardar(transportista);
		 
	    
	    return ResponseEntity.ok(transportista);
	  }

}
