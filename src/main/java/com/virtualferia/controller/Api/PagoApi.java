package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Pago;
import com.virtualferia.models.service.IPagoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/pago")
public class PagoApi {
	
	@Autowired
	IPagoService pagoService;
	
	@GetMapping("listar")
    public List<Pago> ListarProductos(){
        return pagoService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Pago> ObtenerPorId(@PathVariable(value = "id") Long pagoId)
	  {
		Pago pago = pagoService.buscarPorId(pagoId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (pago != null) {
			 return ResponseEntity.ok().body(pago);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Pago pago) {
		pagoService.guardar(pago);
	  }
	
	@PostMapping("pagar")
	  public void Pagar(@Valid @RequestBody Pago pago) {
		pagoService.pagar(pago);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Pago> Actualizar(
	      @PathVariable(value = "id") Long pagoId, @Valid @RequestBody Pago pagoIn)
	       {
		 
		Pago pago = pagoService.buscarPorId(pagoId);
		
		pago.setFechaRegistro(pagoIn.getFechaRegistro()); 
		pago.setMonto(pagoIn.getMonto()); 
		//pago.setUsuario(pagoIn.getUsuario());
		
		
		pagoService.guardar(pago);
	    	
	    return ResponseEntity.ok(pago);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long pagoId) throws Exception {
	    Pago pago = pagoService.buscarPorId(pagoId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (pago != null) {
	    	 pagoService.eliminar(pagoId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	   
	    
	    
	    
	    return response;
	  }

}
