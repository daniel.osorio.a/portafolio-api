package com.virtualferia.controller.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtualferia.models.entity.Comprador;
import com.virtualferia.models.service.ICompradorService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/comprador")
public class CompradorApi {
	
	
	@Autowired
	ICompradorService compradorService;
	
	@GetMapping("listar")
    public List<Comprador> ListarProductos(){
        return compradorService.listar();
    }
	
	@GetMapping("buscar/{id}")
	  public ResponseEntity<Comprador> ObtenerPorId(@PathVariable(value = "id") Long compradorId)
	  {
		Comprador comprador = compradorService.buscarPorId(compradorId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (comprador != null) {
			 return ResponseEntity.ok().body(comprador);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@GetMapping("buscarPorUserId/{id}")
	  public ResponseEntity<Comprador> ObtenerPorUsuarioId(@PathVariable(value = "id") Long compradorId)
	  {
		Comprador comprador = compradorService.buscarPorUsuarioId(compradorId);
	            //.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		
		if (comprador != null) {
			 return ResponseEntity.ok().body(comprador);
		}
		
	    return ResponseEntity.notFound().build();
	    
	    
	  }
	
	@PostMapping("guardar")
	  public void Crear(@Valid @RequestBody Comprador comprador) {
		compradorService.guardar(comprador);
	  }
	
	@PutMapping("actualizar/{id}")
	  public ResponseEntity<Comprador> Actualizar(
	      @PathVariable(value = "id") Long productorId, @Valid @RequestBody Comprador compradorIn)
	       {
		 
		Comprador comprador = compradorService.buscarPorId(productorId);
		
		comprador.setVentas(compradorIn.getVentas());
		
		compradorService.guardar(comprador);
	    	
	    return ResponseEntity.ok(comprador);
	  }
	
	@DeleteMapping("eliminar/{id}")
	  public Map<String, Boolean> elimina(@PathVariable(value = "id") Long compradorId) throws Exception {
		Comprador comprador = compradorService.buscarPorId(compradorId);
	            // .orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
	    
	    
	    Map<String, Boolean> response = new HashMap<>();
	    
	    if (comprador != null) {
	    	compradorService.eliminar(compradorId);
	    	 response.put("eliminado", Boolean.TRUE);
		}else {
			 response.put("No eliminado", Boolean.FALSE);
		}
	   
	    
	    
	    
	    return response;
	  }

}
