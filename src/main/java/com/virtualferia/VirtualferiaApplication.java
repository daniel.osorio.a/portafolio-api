package com.virtualferia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling  // Habilita tareas Programadas
public class VirtualferiaApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualferiaApplication.class, args);
    }

}
